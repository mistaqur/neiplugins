package mods.neiplugins;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.ModContainer;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLInterModComms.IMCEvent;
import cpw.mods.fml.common.event.FMLInterModComms.IMCMessage;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.versioning.ArtifactVersion;
import cpw.mods.fml.common.versioning.VersionParser;

import mods.neiplugins.common.IMCHandler;
import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.Utils;
import mods.neiplugins.options.OptionsNEIPlugins;

@Mod(modid = "NEIPlugins", name = "NEI Plugins", version = NEIPlugins.VERSION, dependencies = "@DEPENDENCIES@")

public class NEIPlugins
{
	public static final String VERSION = "@VERSION@";
	public static Logger logger = Logger.getLogger("NEIPlugins");

	@Mod.Instance("NEIPlugins")
	private static NEIPlugins instance;
	private ModMetadata modMetadata;

	public File modLocation;
	public Map<String, IPlugin> plugins;
	public Map<String, EnumPluginState> pluginsState;
	public Map<String, ArtifactVersion> modVersions;

	static
	{
		logger.setParent(FMLCommonHandler.instance().getFMLLogger());
	}

	public static NEIPlugins getMod()
	{
		return instance;
	}

	public boolean hasMod(ArtifactVersion modVersion)
	{
		return modVersions.containsKey(modVersion.getLabel()) && modVersion.containsVersion(modVersions.get(modVersion.getLabel()));
	}

	public boolean hasMod(String modVersion)
	{
		return hasMod(VersionParser.parseVersionReference(modVersion));
	}

	public static void log(Level level, String msg, Object... params) {
		logger.log(level, Utils.logFormat(msg, params));
	}

	public static void logEx(Level level, String msg, Exception ex, Object... params) {
		logger.log(level, Utils.logFormat(msg, params), ex);
	}

	public static void logWarningEx(String msg, Exception ex, Object... params) {
		logEx(Level.WARNING, msg, ex, params);
	}

	public static void logWarning(String msg, Object... params) {
		log(Level.WARNING, msg, params);
	}
	public static void logInfo(String msg, Object... params) {
		log(Level.INFO, msg, params);
	}
	public static void logFine(String msg, Object... params) {
		log(Level.FINE, msg, params);
	}

	private void loadPlugins()
	{
		ClassLoader classLoader = NEIPlugins.class.getClassLoader();

		if (modLocation.isFile() && (modLocation.getName().endsWith(".jar") || modLocation.getName().endsWith(".zip"))) {
			loadPluginsFromFile(modLocation, classLoader);
		} else if (modLocation.isDirectory()) {
			loadPluginsFromBin(modLocation, modLocation.getPath()+File.separatorChar, classLoader);
		}

	}
	private void loadPluginsFromFile(File file, ClassLoader classLoader) {
		String pluginName;
		try {
			ZipEntry entry = null;
			FileInputStream fileIO = new FileInputStream(modLocation);
			ZipInputStream zipIO = new ZipInputStream(fileIO);

			while (true) {
				entry = zipIO.getNextEntry();

				if (entry == null) {
					fileIO.close();
					break;
				}

				String entryName = entry.getName();
				File entryFile = new File(entryName);
				pluginName = entryFile.getName();
				if (!entry.isDirectory() && pluginName.startsWith("NEIPlugins_") && pluginName.endsWith(".class") && pluginName.indexOf('$')==-1)
					this.addPlugin(classLoader, pluginName, entryFile.getParent());
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void loadPluginsFromBin(File bin, String basePath, ClassLoader classLoader) {
		String pluginName;
		String dirPath;
		File[] fileList = bin.listFiles();

		if (fileList != null)
			for (File file : fileList) {
				pluginName = file.getName();
				if (file.isFile() && pluginName.startsWith("NEIPlugins_") && pluginName.endsWith(".class") && pluginName.indexOf('$')==-1) {
					dirPath = file.getParent();
					if (dirPath.startsWith(basePath)) {
						dirPath = dirPath.substring(basePath.length());
						addPlugin(classLoader, pluginName, dirPath);
					}
				} else if (file.isDirectory())
					loadPluginsFromBin(file, basePath, classLoader);
			}
	}

	private void updateModMetadata()
	{
		String pluginslist = "Available plugins:";
		for (String pluginName : pluginsState.keySet()) {
			IPlugin plugin = plugins.get(pluginName);
	                pluginslist += "\n " + pluginsState.get(pluginName).getColorPrefix() + plugin.getPluginName() + " ("+plugin.getPluginVersion()+")";
		}
		if (pluginsState.size() > 0)
			modMetadata.description = pluginslist;
		else
			modMetadata.description = "No plugins detected.";
	}

	public void addPlugin(ClassLoader classLoader, String pluginName, String dirPath) {
		if (dirPath == null) {
			logWarning("Invalid addPlugin call for plugin \"{0}\"", pluginName);
			return;
		}
		String pluginClassName = dirPath.replace(File.separatorChar, '.')+'.'+pluginName.split("\\.")[0];
		try {
			Class<?> pluginClass = classLoader.loadClass(pluginClassName);
			if (pluginClass != null) {
				Object obj = pluginClass.newInstance();
				if (!(obj instanceof IPlugin))
					return;
				IPlugin plugin = (IPlugin) obj;
				logFine("Found NEIPlugins plugin {0} ({1})", plugin.getPluginName(), plugin.getPluginVersion());
				plugins.put(plugin.getPluginName(),plugin);
			}
		} catch (NoClassDefFoundError ex) {
			logWarning("Error while detecting plugin: Class {0} not found " + pluginName,ex.getMessage());
		} catch (Exception ex) {
			logWarningEx("Error while detecting plugin " + pluginName,ex);
		}
	}

	public void initPlugins() {
		pluginsState.clear();

		for (IPlugin plugin : plugins.values()) {
			pluginsState.put(plugin.getPluginName(), EnumPluginState.INACTIVE);
			try {
				if (!plugin.isValid()) {
					logFine("Plugin {0} ({1}) not active", plugin.getPluginName(), plugin.getPluginVersion());
					continue;
				}
			} catch (NoClassDefFoundError ex) {
				logWarning("Error while calling isValid() for plugin \"{0} ({1})\": {2}", plugin.getPluginName(), plugin.getPluginVersion(), ex.getMessage());
				pluginsState.put(plugin.getPluginName(), EnumPluginState.ERROR);
				continue;
			} catch (Exception ex) {
				logWarningEx("Error while calling isValid() for plugin \"{0} ({1})\"", ex, plugin.getPluginName(), plugin.getPluginVersion());
				pluginsState.put(plugin.getPluginName(), EnumPluginState.ERROR);
				continue;
			}

			try {
				plugin.init();
				pluginsState.put(plugin.getPluginName(), EnumPluginState.POSTINIT);
			} catch (NoClassDefFoundError ex) {
				logWarning("Error while calling init() for plugin \"{0} ({1})\": {2}", plugin.getPluginName(), plugin.getPluginVersion(), ex.getMessage());
				pluginsState.put(plugin.getPluginName(), EnumPluginState.ERROR);
			} catch (Exception ex) {
				logWarningEx("Error while calling init() for plugin \"{0} ({1})\"", ex, plugin.getPluginName(), plugin.getPluginVersion());
				pluginsState.put(plugin.getPluginName(), EnumPluginState.ERROR);
			}
		}
		updateModMetadata();
	}

	public void loadConfig() {
		for (String pluginName : pluginsState.keySet()) {
			if (pluginsState.get(pluginName) != EnumPluginState.POSTINIT)
				continue;

			IPlugin plugin = plugins.get(pluginName);

			try {
				plugin.loadConfig();
				pluginsState.put(pluginName, EnumPluginState.CLIENTINIT);
			} catch (NoClassDefFoundError ex) {
				logWarning("Error while calling loadConfig() for plugin \"{0} ({1})\": {2}", plugin.getPluginName(), plugin.getPluginVersion(), ex.getMessage());
				pluginsState.put(pluginName, EnumPluginState.ERROR);
			} catch (Exception ex) {
				logWarningEx("Error while calling loadConfig() for plugin \"{0} ({1})\"", ex, plugin.getPluginName(), plugin.getPluginVersion());
				pluginsState.put(pluginName, EnumPluginState.ERROR);
			}
		}
		updateModMetadata();

		OptionsNEIPlugins.addOptions();
	}


	@Mod.PreInit
	public void preInit(FMLPreInitializationEvent evt)
	{
		modLocation = evt.getSourceFile();
		plugins = new HashMap<String, IPlugin>();
		pluginsState = new HashMap<String, EnumPluginState>();
		modVersions = new HashMap<String, ArtifactVersion>();
	}

	@Mod.Init
	public void init(FMLInitializationEvent event)
	{
		modMetadata = Loader.instance().activeModContainer().getMetadata();
	}

	@Mod.PostInit
	public void postInit(FMLPostInitializationEvent evt)
	{
		for (ModContainer mod : Loader.instance().getActiveModList())
			modVersions.put(mod.getModId(), mod.getProcessedVersion());

		loadPlugins();
		initPlugins();
		IMCHandler.postInit();
	}

	@Mod.IMCCallback
	public void processIMCMessages(IMCEvent event) {
		for (IMCMessage message : event.getMessages()) {
			IMCHandler.processIMCMessage(message);
		}
	}

	public enum EnumPluginState {
		ERROR("\247c"), INACTIVE("\2477"), POSTINIT("\2476"), CLIENTINIT("\247a");

		private final String colorPrefix;

		private EnumPluginState(String colorPrefix)
		{
			this.colorPrefix = colorPrefix;
		}
		public String getColorPrefix()
		{
			return colorPrefix;
		}

	}
}