package mods.neiplugins.mfr;

import java.awt.Dimension;
import java.awt.Point;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandom;
import net.minecraft.util.WeightedRandomItem;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;

import mods.neiplugins.common.Utils;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.IListElement;
import mods.neiplugins.lists.MultipleStackListElement;
import powercrystals.core.random.WeightedRandomItemStack;
import powercrystals.minefactoryreloaded.MFRRegistry;

public class GuiLaserDrillList extends GuiList
{
	protected GuiLaserDrillList(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}

	@Override
	public int getElementsOffset()
	{
		return 20;
	}

	@Override
        public int getElementHeight()
        {
		return 20;
        }

	@Override
	protected void drawBackground(int index, boolean empty, boolean hovered)
	{
	}

	public static boolean showList(String title)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		List<WeightedRandomItem> recipes = MFRRegistry.getLaserOres();
		int totalWeight = WeightedRandom.getTotalWeight(recipes);

		Dimension eSize = new Dimension(140,16);

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		Iterator<WeightedRandomItem> stacks = recipes.iterator();

		while (stacks.hasNext())
		{
			list.add(new ListElement1(stacks, totalWeight, 4, eSize));
		}

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiLaserDrillList(prevscreen, title, list));
		return true;
	}

	static public class ListElement1 extends MultipleStackListElement
	{
		public ListElement1(Iterator<WeightedRandomItem> stacks, int totalWeight, int margin, Dimension size)
		{
			int pos = 1;
			this.margin = margin;
			this.stacks = new ArrayList<PositionedStack>();
			this.values = new ArrayList<Float>();
			WeightedRandomItem entry;
			while ((pos + 16 < size.width) && stacks.hasNext()) {
				entry = stacks.next();
				if (!(entry instanceof WeightedRandomItemStack)) {
					continue;
				}
				ItemStack stack = ((WeightedRandomItemStack)entry).getStack();
				if (!Utils.isSafeItemStack(stack, "mistaqur.nei.mfr.GuiLaserDrillList"))
					continue;
				this.stacks.add(new PositionedStack(stack,pos,1));
				if (totalWeight>0)
					this.values.add(1.0f*entry.itemWeight/totalWeight);
				else
					this.values.add(0.f);
				pos += 16 + margin;
			}
		}

		@Override
		protected List<String> handleItemTooltip(GuiList gui, int index, List<String> currenttip, Point mousePos)
		{
			currenttip.add("Chance: "+chanceFormat.format(values.get(index)));
			return currenttip;
		}

		public ArrayList<Float> values;
	}

	public static DecimalFormat chanceFormat = new DecimalFormat("0.##%");

}