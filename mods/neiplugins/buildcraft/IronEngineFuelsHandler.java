package mods.neiplugins.buildcraft;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.NEIClientConfig;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler;

import buildcraft.api.fuels.IronEngineCoolant;
import buildcraft.api.fuels.IronEngineFuel;
import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;

public class IronEngineFuelsHandler extends TemplateRecipeHandler
{
	static Rectangle fuelTank = new Rectangle (99,3,16,58);
	static Rectangle coolantTank = new Rectangle (117,3,16,58);
	static Point overlayTank = new Point(176,0);

	public class CachedIronEngineRecipeFuel extends CachedIronEngineRecipe
	{
		public CachedIronEngineRecipeFuel(IronEngineFuel fuel, LiquidStack liquid)
		{
			liquid.amount = 10000;
			this.fueltank = new IronEngineFuelLiquidTank(fuel, 10000, fuelTank,overlayTank);
			this.coolanttank = new LiquidTank(liquid, 10000, coolantTank,overlayTank);
			this.fueltank.showAmount = false;
			this.coolanttank.showAmount = false;
			this.coolanttank.overrideTitle = "Coolant";

			PositionedStack[] input_cont = LiquidHelper.generateStacksForLiquid(this.fueltank.liquid,47,9,47,25);

			slot_input = new ArrayList<PositionedStack>();
			if (input_cont[1] != null)
				slot_input.add(input_cont[1]);
		}
	}

	public class CachedIronEngineRecipeCoolant extends CachedIronEngineRecipe
	{
		public CachedIronEngineRecipeCoolant(IronEngineCoolant coolant, LiquidStack liquid)
		{
			liquid.amount = 10000;
			this.fueltank = new LiquidTank(liquid, 10000,fuelTank,overlayTank);
			this.coolanttank = new IronEngineCoolantLiquidTank(coolant,10000,coolantTank,overlayTank);
			this.fueltank.showAmount = false;
			this.coolanttank.showAmount = false;
			this.fueltank.overrideTitle = "Fuel";

			PositionedStack[] input_cont = LiquidHelper.generateStacksForLiquid(this.coolanttank.liquid,47,9,47,25);

			slot_input = new ArrayList<PositionedStack>();
			if (input_cont[1] != null)
				slot_input.add(input_cont[1]);
		}
	}

	public class CachedIronEngineRecipe extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		public CachedIronEngineRecipe () {}

		@Override
		public ArrayList<PositionedStack> getIngredients()
		{
			return getCycledIngredients(cycleticks / 20, slot_input);
		}

		@Override
		public PositionedStack getResult()
		{
			return null;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			ArrayList<LiquidTank> res = new ArrayList<LiquidTank>();
			res.add(fueltank);
			res.add(coolanttank);
			return res;
		}

		ArrayList<PositionedStack> slot_input;

		LiquidTank fueltank;
		LiquidTank coolanttank;
	}



	@Override
	public String getRecipeName()
	{
		return "Combustion Engine";
	}

	public boolean hasOverlay(GuiContainer gui, Container container, int recipe)
	{
		return false;
	}

	public void drawLiquidTanks(GuiContainerManager gui, int recipe)
	{
		CachedRecipe crecipe = arecipes.get(recipe);
		if (crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				tank.draw(gui, getGuiTexture());
			}
		}
	}


	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(147, 11, 12, 12), "buildcraft.ironengine.coolant"));
		transferRects.add(new RecipeTransferRect(new Rectangle(147, 27, 12, 12), "buildcraft.ironengine.fuel"));
	}

	public void drawBackground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.bindTexture(getGuiTexture());
		gui.drawTexturedModalRect(0, 0, 5, 16, 166, 65);
	}

	@Override
	public void drawForeground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_LIGHTING);
		gui.bindTexture("/gfx/buildcraft/gui/triggers.png");
		if (recipe % 2 == 0 ) {
			gui.drawTexturedModalRect(150, 30, 5, 21, 6, 6);
			gui.drawTexturedModalRect(150, 14, 37, 21, 6, 6);
		}

		gui.bindTexture(getGuiTexture());
	        drawExtras(gui, recipe);
		drawLiquidTanks(gui, recipe);
	}

	@Override
	public void loadUsageRecipes(String inputId, Object... ingredients)
	{
		if (inputId.equals("liquid") && ingredients.length == 1 && ingredients[0] instanceof LiquidStack)
		{
			LiquidStack t = (LiquidStack)ingredients[0];
			IronEngineFuel fuel = IronEngineFuel.getFuelForLiquid(t);
			if (fuel != null)
				arecipes.add(new CachedIronEngineRecipeFuel(fuel, IronEngineCoolant.coolants.get(0).liquid));
			IronEngineCoolant coolant = IronEngineCoolant.getCoolantForLiquid(t);
			if (coolant != null)
				arecipes.add(new CachedIronEngineRecipeCoolant(coolant, IronEngineFuel.fuels.get(0).liquid));
		}
		else
		{
			super.loadUsageRecipes(inputId, ingredients);
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(ingredient);
		if (t != null) {
			IronEngineFuel fuel = IronEngineFuel.getFuelForLiquid(t);
			if (fuel != null)
				arecipes.add(new CachedIronEngineRecipeFuel(fuel, IronEngineCoolant.coolants.get(0).liquid));
			IronEngineCoolant coolant = IronEngineCoolant.getCoolantForLiquid(t);
			if (coolant != null)
				arecipes.add(new CachedIronEngineRecipeCoolant(coolant, IronEngineFuel.fuels.get(0).liquid));
		}
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals("buildcraft.ironengine.fuel") && getClass() == IronEngineFuelsHandler.class)
		{
			for (IronEngineCoolant coolant : IronEngineCoolant.coolants)
			{
				arecipes.add(new CachedIronEngineRecipeCoolant(coolant, IronEngineFuel.fuels.get(0).liquid));
			}
		}
		else if(outputId.equals("buildcraft.ironengine.coolant") && getClass() == IronEngineFuelsHandler.class)
		{
			for (IronEngineFuel fuel : IronEngineFuel.fuels)
			{
				arecipes.add(new CachedIronEngineRecipeFuel(fuel, IronEngineCoolant.coolants.get(0).liquid));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	@Override
	public String getGuiTexture()
	{
		return "/gfx/buildcraft/gui/combustion_engine_gui.png";
	}

	public class IronEngineCoolantLiquidTank extends LiquidTank {
		public IronEngineCoolantLiquidTank(IronEngineCoolant coolant, int capacity, Rectangle position, Point overlay)
		{
			super(coolant.liquid, capacity, position);
			this.liquid.amount = capacity;
			this.coolingPerUnit = coolant.coolingPerUnit;
			this.overlay = overlay;
		}
		@Override
		public List<String> additionalHandleTooltip(List<String> currenttip)
		{
			if (liquid.itemID > 0)
			{
				currenttip.add("\u00a77Coolant: "+coolingPerUnit+"");
			}
			return currenttip;
		}

		public float coolingPerUnit;
	}

	public class IronEngineFuelLiquidTank extends LiquidTank {
		public IronEngineFuelLiquidTank(IronEngineFuel fuel, int capacity, Rectangle position, Point overlay)
		{
			super(fuel.liquid, capacity, position);
			this.liquid.amount = capacity;
			this.powerPerCycle = fuel.powerPerCycle;
			this.totalBurningTime = fuel.totalBurningTime;
			this.overlay = overlay;
		}
		@Override
		public List<String> additionalHandleTooltip(List<String> currenttip)
		{
			if (liquid.itemID > 0)
			{
				currenttip.add("\u00a77Stats per bucket:");
				currenttip.add("\u00a77Output: "+powerPerCycle+" MJ/t");
				currenttip.add("\u00a77Duration: "+totalBurningTime+" t");
			}
			return currenttip;
		}

		public float powerPerCycle;
		public int totalBurningTime;
	}

	protected boolean transferLiquidTank(GuiRecipe guiRecipe, int recipe, boolean usage)
	{
		CachedRecipe crecipe = arecipes.get(recipe);
		if (crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			Point mousepos = guiRecipe.manager.getMousePosition();
			Point offset = guiRecipe.getRecipePosition(recipe);
			Point relMouse = new Point(mousepos.x - guiRecipe.guiLeft - offset.x, mousepos.y - guiRecipe.guiTop - offset.y);

			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				if (tank.position.contains(relMouse))
				{
					if (tank.liquid.itemID > 0 && (usage ?
							GuiUsageRecipe.openRecipeGui("liquid", tank.liquid) :
							GuiCraftingRecipe.openRecipeGui("liquid", tank.liquid)))
						return true;
				}
			}
		}
		return false;
	}

	@Override
	public List<String> handleTooltip(GuiRecipe guiRecipe, List<String> currenttip, int recipe)
	{
		currenttip = super.handleTooltip(guiRecipe, currenttip, recipe);
		CachedRecipe crecipe = arecipes.get(recipe);
		if (guiRecipe.manager.shouldShowTooltip() && crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			Point mousepos = guiRecipe.manager.getMousePosition();
			Point offset = guiRecipe.getRecipePosition(recipe);
			Point relMouse = new Point(mousepos.x - guiRecipe.guiLeft - offset.x, mousepos.y - guiRecipe.guiTop - offset.y);

			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				if (tank.position.contains(relMouse))
				{
					tank.handleTooltip(currenttip);
				}
			}
		}
		return currenttip;
	}

	@Override
	public boolean keyTyped(GuiRecipe gui, char keyChar, int keyCode, int recipe)
	{
		if(keyCode == NEIClientConfig.getKeyBinding("recipe"))
		{
			if (transferLiquidTank(gui, recipe, false))
				return true;
		}
		else if(keyCode == NEIClientConfig.getKeyBinding("usage"))
		{
			if (transferLiquidTank(gui, recipe, true))
				return true;
		}
		return super.keyTyped(gui, keyChar, keyCode, recipe);
	}

	@Override
	public boolean mouseClicked(GuiRecipe gui, int button, int recipe)
	{
		if(button == 0)
		{
			if (transferLiquidTank(gui, recipe, false))
				return true;
		}
		else if(button == 1)
		{
			if (transferLiquidTank(gui, recipe, true))
				return true;
		}
		return super.mouseClicked(gui, button, recipe);
	}

}