package mods.neiplugins.buildcraft;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.TemplateRecipeHandler;

import buildcraft.api.recipes.RefineryRecipe;
import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;
import mods.neiplugins.common.LiquidTemplateRecipeHandler;

public class RefineryRecipeHandler extends LiquidTemplateRecipeHandler
{
	static final int yoffset = 20;
	static Rectangle input1Tank = new Rectangle (33, 43 - yoffset, 16, 16);
	static Rectangle input2Tank = new Rectangle (121, 43 - yoffset, 16, 16);
	static Rectangle outputTank = new Rectangle (77, 43 - yoffset, 16, 16);
	static Point overlayTank = new Point(177,1);

	public class CachedRefineryRecipe extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		public CachedRefineryRecipe(RefineryRecipe recipe)
		{
			tanks = new ArrayList<LiquidTank>();
			LiquidTank tank;

			tank = new LiquidTank(recipe.result, 4000, outputTank);
			tank.ignoreCapacity = true;
			tank.showMillBuckets = true;
			tanks.add(tank);

			tank = new LiquidTank(recipe.ingredient1, 4000, input1Tank);
			tank.ignoreCapacity = true;
			tank.showMillBuckets = true;
			tanks.add(tank);

			tank = new LiquidTank(recipe.ingredient2, 4000, input2Tank);
			tank.ignoreCapacity = true;
			tank.showMillBuckets = true;
			tanks.add(tank);

			energy = recipe.energy;
			delay = recipe.delay;
		}

		@Override
		public PositionedStack getResult()
		{
			return null;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			return tanks;
		}

		int energy;
		int delay;
		ArrayList<LiquidTank> tanks;
	}

	@Override
	public String getRecipeId()
	{
		return "buildcraft.refinery";
	}

	@Override
	public String getRecipeName()
	{
		return "Refinery";
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(52, 44 - yoffset, 23, 15), getRecipeId()));
		transferRects.add(new RecipeTransferRect(new Rectangle(96, 44 - yoffset, 23, 15), getRecipeId()));
	}

	public Collection<RefineryRecipe> getRecipes()
	{
		return RefineryRecipe.getRecipes();
	}

	@Override
	public void loadCraftingRecipes(LiquidStack result)
	{
		for (RefineryRecipe irecipe : getRecipes())
			if(LiquidHelper.areSameLiquid(result, irecipe.result))
				arecipes.add(new CachedRefineryRecipe(irecipe));
	}

	@Override
	public void loadUsageRecipes(LiquidStack ingredient)
	{
		for (RefineryRecipe irecipe : getRecipes())
			if(LiquidHelper.areSameLiquid(ingredient, irecipe.ingredient1) || LiquidHelper.areSameLiquid(ingredient, irecipe.ingredient2))
				arecipes.add(new CachedRefineryRecipe(irecipe));
	}

	@Override
	public void loadSameRecipeId()
	{
		for (RefineryRecipe irecipe : getRecipes())
			arecipes.add(new CachedRefineryRecipe(irecipe));
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(result);
		if (t != null)
			loadCraftingRecipes(t);
	}
	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(ingredient);
		if (t != null)
			loadUsageRecipes(t);
	}

	@Override
	public void drawBackground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.bindTexture(getGuiTexture());
		gui.drawTexturedModalRect(0, 0, 5, 11 + yoffset, 166, 65);
	}

	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		int energy = ((CachedRefineryRecipe)arecipes.get(recipe)).energy;
		int delay = ((CachedRefineryRecipe)arecipes.get(recipe)).delay;
		gui.drawTextCentered("Require "+energy+" MJ each " + ((delay > 1)? delay + " ticks":"tick"), 82, 65 - yoffset, 0xFF808080, false);
	}

	@Override
	public String getGuiTexture()
	{
		return "/gfx/buildcraft/gui/refinery_filter.png";
	}
}