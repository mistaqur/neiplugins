package mods.neiplugins.forge;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.TemplateRecipeHandler;
import codechicken.nei.recipe.TemplateRecipeHandler.CachedRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler.RecipeTransferRect;

import mods.neiplugins.common.Utils;

public class OreDictionaryList extends TemplateRecipeHandler
{
	public class CachedOreDictionaryEntry extends CachedRecipe
	{
		public String name;
		public ArrayList<PositionedStack> content;

		public CachedOreDictionaryEntry(ItemStack ore, String name) {
			this.name = name;
			this.content = new ArrayList();

			this.content.add(new PositionedStack(ore, 51, 24));
			this.content.add(new PositionedStack(OreDictionary.getOres(name), 111, 24));
		}

		public ArrayList getIngredients()
		{
			return getCycledIngredients(OreDictionaryList.this.cycleticks / 20, this.content);
		}

		public PositionedStack getResult()
		{
			return null;
		}
	}

	public String getRecipeName()
	{
		return "Ore Dictionary";
	}

	public String getRecipeId()
	{
		return "forge.oredictionary";
	}

	public String getGuiTexture()
	{
		return "/mods/neiplugins/gfx/Item2Item.png";
	}

	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		gui.drawTextCentered(((CachedOreDictionaryEntry)this.arecipes.get(recipe)).name, 85, 9, -8355712, false);
	}

	public void loadTransferRects()
	{
		this.transferRects.add(new RecipeTransferRect(new Rectangle(74, 23, 25, 16), getRecipeId()));
	}

	protected ArrayList<CachedOreDictionaryEntry> getAllItems()
	{
		ArrayList<CachedOreDictionaryEntry> result = new ArrayList<CachedOreDictionaryEntry>();
		String[] oreNames = OreDictionary.getOreNames();
		Arrays.sort(oreNames);
		for (String oreName : oreNames) {
			for (ItemStack ore : OreDictionary.getOres(oreName))
			{
				result.add(new CachedOreDictionaryEntry(ore, oreName));
			}
		}
		return result;
	}

	protected ArrayList<CachedOreDictionaryEntry> getItemsByID(int id)
	{
		ArrayList<CachedOreDictionaryEntry> result = new ArrayList<CachedOreDictionaryEntry>();
		for (ItemStack ore : OreDictionary.getOres(id))
			result.add(new CachedOreDictionaryEntry(ore, OreDictionary.getOreName(id)));
		return result;
	}

	public void loadCraftingRecipes(String outputId, Object[] results)
	{
		if (outputId.equals(getRecipeId()))
		{
			for (CachedOreDictionaryEntry recipe : getAllItems())
				this.arecipes.add(recipe);
		}
		else
			super.loadCraftingRecipes(outputId, results);
	}

	public void loadUsageRecipes(ItemStack ingredient)
	{
		if (!Utils.getBooleanSetting("mistaqur.showUsageInOreDictionary"))
			return;
		int oreID = OreDictionary.getOreID(ingredient);
		if (oreID != -1)
			for (CachedOreDictionaryEntry recipe : getItemsByID(oreID))
				if (recipe.contains(recipe.content, ingredient))
					this.arecipes.add(recipe);
	}

}