package mods.neiplugins.forge;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;

import codechicken.nei.NEIClientUtils;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import mods.neiplugins.NEIPlugins;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.IListElement;
import mods.neiplugins.lists.SimpleItemStackListElement;
import mods.neiplugins.lists.SimpleListElement;

public class GuiGameRegistryModObjectTableList extends GuiList
{
	protected GuiGameRegistryModObjectTableList(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}

	public static ImmutableTable<String, String, Integer> getModObjectTable()
	{
		try {
			Class gameDataCls = Class.forName("cpw.mods.fml.common.registry.GameData");
			Field field = gameDataCls.getDeclaredField("modObjectTable");
			field.setAccessible(true);
			return (ImmutableTable<String, String, Integer>) field.get(null);
		} catch (Exception ex) {
			NEIPlugins.logInfo("Failed to get field \"modObjectTable\" from class \"cpw.mods.fml.common.registry.GameData\": {0}", ex.getMessage());
		}
		return null;
	}

	public static boolean showList(String title)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		for (String modId : new TreeSet<String>(getModObjectTable().rowKeySet()))
		{
			list.add(new ListElement1(modId));
		}

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiGameRegistryModObjectTableList(prevscreen, title, list));
		return true;
	}

	public static boolean showModRecipeList(String mod)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;
		ImmutableTable<String, String, Integer> modObjectTable = getModObjectTable();

		if (!modObjectTable.containsRow(mod))
			return false;

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		Map<String,Integer> map = modObjectTable.row(mod);
		for (String key : new TreeSet<String>(map.keySet()))
		{
			list.add(new SimpleItemStackListElement(key,new ItemStack(map.get(key), 1, 0)));
		}

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiGameRegistryModObjectTableList(prevscreen, mod, list));
		return true;
	}

	static class ListElement1 extends SimpleListElement
	{
		ListElement1(String title)
		{
			super(title);
		}
		@Override
		public boolean click(int button)
		{
			return GuiGameRegistryModObjectTableList.showModRecipeList(this.title);
		}
	}


}