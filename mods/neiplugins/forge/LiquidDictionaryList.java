package mods.neiplugins.forge;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.TemplateRecipeHandler;
import codechicken.nei.recipe.TemplateRecipeHandler.CachedRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler.RecipeTransferRect;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;
import mods.neiplugins.common.LiquidTemplateRecipeHandler;
import mods.neiplugins.common.Utils;

public class LiquidDictionaryList extends LiquidTemplateRecipeHandler
{
	static Rectangle liquidTank = new Rectangle (22,8,48,47);

	public class CachedLiquidDictionaryEntry extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		LiquidDictionaryTank tank;
		PositionedStack liquidInput;
		PositionedStack liquidOutput;
		String name;

		public CachedLiquidDictionaryEntry(LiquidStack liquid) {
			this.name = name;

			this.tank = new LiquidDictionaryTank(liquid.copy(),1000,liquidTank);
			this.tank.liquid.amount = 1000;
			this.tank.showAmount = false;
			this.tank.showCapacity = false;

			PositionedStack[] output_cont = LiquidHelper.generateStacksForLiquid(tank.liquid,104,6,104,42);
			liquidInput = output_cont[0];
			liquidOutput = output_cont[1];
		}

		@Override
		public ArrayList<PositionedStack> getOtherStacks()
		{
			ArrayList<PositionedStack> stacks = new ArrayList<PositionedStack>();
			if (liquidInput != null && liquidOutput != null) {
				stacks.add(LiquidHelper.getSeqCycledStack(cycleticks / 20, liquidInput));
				stacks.add(LiquidHelper.getSeqCycledStack(cycleticks / 20, liquidOutput));
			}
			return stacks;
		}

		public boolean sameLiquid(LiquidStack liquid)
		{
			return LiquidHelper.areSameLiquid(tank.liquid, liquid);
		}

		@Override
		public PositionedStack getResult()
		{
			return null;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			ArrayList<LiquidTank> res = new ArrayList<LiquidTank>();
			res.add(tank);
			return res;
		}
	}

	public class LiquidDictionaryTank extends LiquidTank {
		public LiquidDictionaryTank(LiquidStack liquid, int capacity, Rectangle position)
		{
			super(liquid, capacity, position);
			this.dictionaryNames = new ArrayList<String>();
		}
		@Override
		public List<String> additionalHandleTooltip(List<String> currenttip)
		{
			if (Utils.getBooleanSetting("mistaqur.showLiquidDictionaryNames")) {
				if (this.dictionaryNames.size()>0) {
					currenttip.add("\u00a77Liquid dictionary names:");
					for (String name : this.dictionaryNames) {
						currenttip.add("\u00a73"+name);
					}
				}
			}
			if (Utils.getBooleanSetting("mistaqur.showLiquidDictionaryFuelTooltip")) {
				List<String> fuelTooltip = new ArrayList<String>();
				FuelTooltip.addLiquidStackFuelTooltip(currenttip, this.liquid, FuelTooltip.disabledHelpers);
				if (fuelTooltip != null && fuelTooltip.size()>0) {
					currenttip.add("\u00a77Can produce (per bucket):");
					currenttip.addAll(fuelTooltip);
				}
			}
			return currenttip;
		}

		public List<String> dictionaryNames;
	}
	@Override
	public String getRecipeName()
	{
		return "Liquid Registry";
	}

	@Override
	public String getRecipeId()
	{
		return "forge.liquiddictionary";
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/neiplugins/gfx/LiquidDictionaryList.png";
	}

	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		gui.drawTextCentered(((CachedLiquidDictionaryEntry)this.arecipes.get(recipe)).name, 85, 9, -8355712, false);
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new RecipeTransferRect(new Rectangle(74, 23, 25, 16), getRecipeId()));
	}

	protected ArrayList<CachedLiquidDictionaryEntry> getAllRecipes()
	{
		ArrayList<CachedLiquidDictionaryEntry> result = new ArrayList<CachedLiquidDictionaryEntry>();
		Map <List,CachedLiquidDictionaryEntry> liquids = new HashMap<List,CachedLiquidDictionaryEntry>();

		for (LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData())
		{
			if (!liquids.containsKey(Arrays.asList(data.stillLiquid.itemID, data.stillLiquid.itemMeta))) {
				CachedLiquidDictionaryEntry entry = new CachedLiquidDictionaryEntry(data.stillLiquid);
				liquids.put(Arrays.asList(data.stillLiquid.itemID, data.stillLiquid.itemMeta),entry);
				result.add(entry);
			}
		}

		Map<String, LiquidStack> definedLiquids = LiquidDictionary.getLiquids();
		if (definedLiquids != null) {
			String[] names = definedLiquids.keySet().toArray(new String[0]);
			for (String name : names) {
				LiquidStack liquid = definedLiquids.get(name);
				CachedLiquidDictionaryEntry entry = liquids.get(Arrays.asList(liquid.itemID, liquid.itemMeta));
				if (entry == null) {
					entry = new CachedLiquidDictionaryEntry(liquid);
					liquids.put(Arrays.asList(liquid.itemID, liquid.itemMeta),entry);
					result.add(entry);
				}
				entry.tank.dictionaryNames.add(name);
			}
		}

		return result;
	}

	@Override
	public void loadUsageRecipes(LiquidStack ingredient)
	{
		for (CachedLiquidDictionaryEntry recipe : getAllRecipes())
			if (recipe.sameLiquid(ingredient))
				this.arecipes.add(recipe);
	}

	@Override
	public void loadSameRecipeId()
	{
		for (CachedLiquidDictionaryEntry recipe : getAllRecipes())
		{
			this.arecipes.add(recipe);
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(ingredient);
		if (t != null)
			for (CachedLiquidDictionaryEntry recipe : getAllRecipes())
				if (recipe.sameLiquid(t))
					this.arecipes.add(recipe);
	}
}