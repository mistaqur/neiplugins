package mods.neiplugins.options;

import mods.neiplugins.common.Registry;
import mods.neiplugins.ic2.GuiRecyclerBlacklist;
import mods.neiplugins.ic2.GuiValuableOreList;
import mods.neiplugins.lists.SimpleListElement;
import mods.neiplugins.options.OptionsIC2;

public abstract class OptionsIC2 {
	public static void addOptions() {
		Registry.infoListMenu.add(new SimpleListElement("IC2 Recycler Blacklist") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiRecyclerBlacklist.showList(this.title):false;
			}
		});

		if (GuiValuableOreList.valuableOres != null) {
			Registry.infoListMenu.add(new SimpleListElement("IC2 Miner Valuable Ores") {
				@Override
				public boolean click(int button)
				{
					return button==0?GuiValuableOreList.showList(this.title):false;
				}
			});
		}
	}
}