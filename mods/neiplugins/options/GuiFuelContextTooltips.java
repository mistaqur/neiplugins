package mods.neiplugins.options;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.NEIClientUtils;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.lists.GuiButtonList;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.IActiveListElement;
import mods.neiplugins.lists.IListElement;
import mods.neiplugins.lists.SimpleListElement;

public class GuiFuelContextTooltips extends GuiButtonList
{
	protected GuiFuelContextTooltips(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}

	static class ListElement1 extends SimpleListElement implements IActiveListElement
	{
		String key;
		ListElement1(String key)
		{
			super(FuelTooltip.contextHelpers.get(key).getName());
			this.key = key;
		}

		@Override
		public List<String> handleTooltip(GuiList gui, List<String> currenttip, Point mousepos)
		{
			List<String> t = FuelTooltip.contextHelpers.get(key).getDescription();
			if (t != null)
				currenttip.addAll(t);
			return currenttip;
		}

		@Override
		public boolean click(int button)
		{
			if (button != 0) return false;
			if (FuelTooltip.disabledContextHelpers.contains(key))
				FuelTooltip.disabledContextHelpers.remove(key);
			else
				FuelTooltip.disabledContextHelpers.add(key);
			return true;
		}

		@Override
		public boolean isActive()
		{
			return !FuelTooltip.disabledContextHelpers.contains(key);
		}

	}

	public static boolean showList(String title)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		for (String key : FuelTooltip.contextHelpers.keySet())
		{
			list.add(new ListElement1(key));
		}

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiFuelContextTooltips(prevscreen, title, list));
		return true;
	}
}