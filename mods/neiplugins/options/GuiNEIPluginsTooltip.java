package mods.neiplugins.options;

import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.GuiNEISettings;

public class GuiNEIPluginsTooltip extends GuiNEISettings
{
	public GuiNEIPluginsTooltip(GuiContainer parentContainer)
	{
		super(parentContainer);
	}

	@Override
	public String getBackButtonName()
	{
		return "NEIPlugins options";
	}

	@Override
	public void onBackButtonClick()
	{
		mc.displayGuiScreen(new GuiNEIPluginsMain(parentScreen));
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();

		updateButtonNames();
	}
}