package mods.neiplugins.options;

import codechicken.nei.GuiNEISettings;
import codechicken.nei.GuiNEISettings.NEIOption;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.api.API;

public abstract class OptionsNEIPlugins {
	public static void addOptions() {
		API.addSetting(new NEIOption("")
		{
			@Override
			public String updateText()
			{
				return "NEIPlugins options";
			}

			@Override
			public void onClick()
			{
				NEIClientUtils.mc().displayGuiScreen(new GuiNEIPluginsMain(((GuiNEISettings)NEIClientUtils.mc().currentScreen).parentScreen));
			}
		}.setGlobalOnly());
		API.addSetting(GuiNEIPluginsMain.class, new NEIOption("")
		{
			@Override
			public String updateText()
			{
				return "Tooltip options";
			}

			@Override
			public void onClick()
			{
				NEIClientUtils.mc().displayGuiScreen(new GuiNEIPluginsTooltip(((GuiNEISettings)NEIClientUtils.mc().currentScreen).parentScreen));
			}
		});
	}
}