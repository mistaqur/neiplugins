package mods.neiplugins.options;

import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.GuiNEISettings;

public class GuiNEIPluginsTooltipFuel extends GuiNEISettings
{
	public GuiNEIPluginsTooltipFuel(GuiContainer parentContainer)
	{
		super(parentContainer);
	}

	@Override
	public String getBackButtonName()
	{
		return "Tooltip options";
	}

	@Override
	public void onBackButtonClick()
	{
		mc.displayGuiScreen(new GuiNEIPluginsTooltip(parentScreen));
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();

		//if(!NEIClientConfig.canDump())
		//	NEIClientConfig.getSetting("ID dump.dump on load").setBooleanValue(false);

		updateButtonNames();
	}
}