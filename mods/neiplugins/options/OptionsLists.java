package mods.neiplugins.options;

import mods.neiplugins.NEIPlugins_Lists;
import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.Registry;
import mods.neiplugins.common.Registry;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.GuiRecipeList;
import mods.neiplugins.lists.SimpleListElement;
import mods.neiplugins.lists.SimpleListElement;
import mods.neiplugins.options.OptionsLists;

public abstract class OptionsLists {
	public static void addOptions() {
		NEIPlugins_Lists.mainMenu.add(new SimpleListElement("Recipe lists") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiRecipeList.showList(this.title):false;
			}
		});

		NEIPlugins_Lists.mainMenu.add(new SimpleListElement("Info lists") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiList.showList(this.title,Registry.infoListMenu):false;
			}
		});

		NEIPlugins_Lists.mainMenu.add(new SimpleListElement("For developers"){
			@Override
			public boolean click(int button)
			{
				return button==0?GuiList.showList(this.title,Registry.developerListMenu):false;
			}
		});

		NEIPlugins_Lists.mainMenu.add(new SimpleListElement("Settings"){
			@Override
			public boolean click(int button)
			{
				return button==0?GuiList.showList(this.title,Registry.settingsListMenu):false;
			}
		});
	}
}