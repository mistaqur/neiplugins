package mods.neiplugins.options;

import java.util.List;
import java.util.Set;

import net.minecraft.item.Item;
import net.minecraftforge.liquids.LiquidDictionary;

import codechicken.nei.GuiNEISettings;
import codechicken.nei.GuiNEISettings.NEIOption;
import codechicken.nei.NEIClientConfig;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.api.API;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.Registry;
import mods.neiplugins.lists.SimpleListElement;
import mods.neiplugins.options.GuiFuelContextTooltips;
import mods.neiplugins.options.GuiFuelTooltips;
import mods.neiplugins.options.GuiNEIPluginsTooltip;
import mods.neiplugins.options.GuiNEIPluginsTooltipFuel;

public abstract class OptionsFuelTooltips {
	public static void addOptions() {
		NEIClientConfig.globalConfig.getTag("mistaqur.showContextTooltip").setDefaultValue("true");
		NEIClientConfig.globalConfig.getTag("mistaqur.showLiquidContainerFuelTooltip").setDefaultValue("true");
		NEIClientConfig.globalConfig.getTag("mistaqur.showLiquiqAsItemFuelTooltip").setDefaultValue("true");
		NEIClientConfig.globalConfig.getTag("mistaqur.showLiquidDictionaryFuelTooltip").setDefaultValue("true");
		NEIClientConfig.globalConfig.getTag("mistaqur.showItemStackFuelTooltip").setDefaultValue("true");

		API.addSetting(GuiNEIPluginsTooltip.class, new NEIOption("")
		{
			@Override
			public String updateText()
			{
				return "Fuel tooltip config";
			}

			@Override
			public void onClick()
			{
				NEIClientUtils.mc().displayGuiScreen(new GuiNEIPluginsTooltipFuel(((GuiNEISettings)NEIClientUtils.mc().currentScreen).parentScreen));
			}
		});

/*		API.addSetting(GuiNEIPluginsTooltipFuel.class, new NEIOption("mistaqur.showFuelTooltip")
			{
				@Override
				public String updateText()
				{
					return ""+(enabled() ? "Show fuel tooltip" : "Hide fuel tooltip");
				}
				public void onClick()
				{
					super.onClick();
				}
			});*/
		API.addSetting(GuiNEIPluginsTooltipFuel.class, new NEIOption("mistaqur.showContextTooltip")
			{
				@Override
				public String updateText()
				{
					return "Context tooltip: "+(enabled() ? "Enabled" : "Disabled");
				}
				public void onClick()
				{
					super.onClick();
					FuelTooltip.updateSettings();
				}
			});
		API.addSetting(GuiNEIPluginsTooltipFuel.class, new NEIOption("mistaqur.showLiquidContainerFuelTooltip")
			{
				@Override
				public String updateText()
				{
					return "Liquid containers: "+(enabled() ? "Shown" : "Hidden");
				}
				public void onClick()
				{
					super.onClick();
					FuelTooltip.updateSettings();
				}
			});
		API.addSetting(GuiNEIPluginsTooltipFuel.class, new NEIOption("mistaqur.showLiquidDictionaryFuelTooltip")
			{
				@Override
				public String updateText()
				{
					return "Liquid Dictionary: "+(enabled() ? "Shown" : "Hidden");
				}
				public void onClick()
				{
					super.onClick();
					FuelTooltip.updateSettings();
				}
			});
		API.addSetting(GuiNEIPluginsTooltipFuel.class, new NEIOption("mistaqur.showItemStackFuelTooltip")
			{
				@Override
				public String updateText()
				{
					return "Items: "+(enabled() ? "Shown" : "Hidden");
				}
				public void onClick()
				{
					super.onClick();
					FuelTooltip.updateSettings();
				}
			});
		FuelTooltip.updateSettings();

		Registry.settingsListMenu.add(new SimpleListElement("Fuel Tooltip") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiFuelTooltips.showList(this.title):false;
			}
		});

		Registry.settingsListMenu.add(new SimpleListElement("Fuel Context Tooltip") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiFuelContextTooltips.showList(this.title):false;
			}
		});
	}
}