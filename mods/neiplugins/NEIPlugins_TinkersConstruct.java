package mods.neiplugins;

import codechicken.nei.api.API;

import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.RecipeHandlerUtils;
import mods.neiplugins.common.Utils;
import mods.neiplugins.tconstruct.AlloySmelteryRecipeHandler;
import mods.neiplugins.tconstruct.LiquidBlockCastingRecipeHandler;
import mods.neiplugins.tconstruct.LiquidCastingRecipeHandler;
import mods.neiplugins.tconstruct.SmelteryRecipeHandler;

public class NEIPlugins_TinkersConstruct implements IPlugin {

	public static final String PLUGIN_NAME = "TConstruct";
	public static final String PLUGIN_VERSION = "1.0.2";
	public static final String REQUIRED_MOD = "TConstruct";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		if (!NEIPlugins.getMod().hasMod(REQUIRED_MOD))
			return false;
		if (Utils.findClass("mods.tinker.tconstruct.library.TConstructRegistry") != null &&
			Utils.findClass("mods.tinker.tconstruct.library.crafting.CastingRecipe")!= null &&
			Utils.findClass("mods.tinker.tconstruct.library.crafting.AlloyMix")!= null &&
			Utils.findClass("mods.tinker.tconstruct.library.crafting.Smeltery")!= null )
			return true;
		NEIPlugins.logInfo("Failed to find necessary classes for mod \"{0}\"",REQUIRED_MOD);
		return false;
	}

	@Override
	public void init() {
	}

	@Override
	public void loadConfig() {
		initHandlers();
	}

	private void initHandlers() {
		API.registerRecipeHandler(new LiquidCastingRecipeHandler());
		API.registerUsageHandler(new LiquidCastingRecipeHandler());
		API.registerRecipeHandler(new LiquidBlockCastingRecipeHandler());
		API.registerUsageHandler(new LiquidBlockCastingRecipeHandler());
		API.registerRecipeHandler(new SmelteryRecipeHandler());
		API.registerUsageHandler(new SmelteryRecipeHandler());
		API.registerRecipeHandler(new AlloySmelteryRecipeHandler());
		API.registerUsageHandler(new AlloySmelteryRecipeHandler());
		RecipeHandlerUtils.addToRecipeList("TConstruct","Smeltery",0,"tconstruct.smeltery");
		RecipeHandlerUtils.addToRecipeList("TConstruct","Alloy Smeltery",0,"tconstruct.alloysmeltery");
		RecipeHandlerUtils.addToRecipeList("TConstruct","Liquid Table Casting",0,"tconstruct.liquidcast");
		RecipeHandlerUtils.addToRecipeList("TConstruct","Liquid Basin Casting",0,"tconstruct.liquidblockcast");
	}
}