package mods.neiplugins.lists;



public interface IActiveListElement
{
	public boolean isActive();
}