package mods.neiplugins.lists;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;

public class RecipeHandlerListElement implements IListElement
{
	public RecipeHandlerListElement(String recipeType, int type, String id, Object[] params)
	{
		this(recipeType, null, type, id, params);
	}

	public RecipeHandlerListElement(String recipeType, ItemStack icon, int type, String id, Object[] params)
	{
		this.recipeType = recipeType;
		this.type = type;
		this.id = id;
		this.params = params;
		if (icon != null) {
			this.stacks = new ArrayList<PositionedStack>();
			this.stacks.add(new PositionedStack(icon,2,2));
		} else {
			this.stacks = null;
		}
	}

	public void draw(GuiContainerManager gui, Dimension size)
	{
		gui.drawTextCentered(this.recipeType, (size.width) / 2, (size.height - 8) / 2, 14737632, false);
	}
	public boolean click(int button)
	{
		if (button == 0)
		{
			return type==1 ? GuiUsageRecipe.openRecipeGui(this.id, this.params) :
					 GuiCraftingRecipe.openRecipeGui(this.id, this.params);
		}
		return false;
	}
	public List<String> handleTooltip(GuiList gui, List<String> currenttip, Point mousePos)
	{
		return currenttip;
	}

	public List<String> handleItemTooltip(GuiList gui, ItemStack stack, List<String> currenttip, Point mousepos)
	{
		return currenttip;
	}

	public ArrayList<PositionedStack> getStacks() {
		return stacks;
	}

	public String recipeType;
	public int type;
	public String id;
	public Object[] params;
	public ArrayList<PositionedStack> stacks;
}