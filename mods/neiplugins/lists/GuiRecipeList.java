package mods.neiplugins.lists;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.NEIClientUtils;

import mods.neiplugins.common.RecipeHandlerUtils;

public class GuiRecipeList extends GuiList
{
	protected GuiRecipeList(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}

	public static boolean showList(String title)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		for (String mod : RecipeHandlerUtils.recipeListMap.keySet())
			list.add(new RecipeModListElement(mod));

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiRecipeList(prevscreen, title, list));
		return true;
	}

	public static boolean showModRecipeList(String mod)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		if(!RecipeHandlerUtils.recipeListMap.containsKey(mod))
			return false;

		NEIClientUtils.overlayScreen(new GuiRecipeList(prevscreen, mod, RecipeHandlerUtils.recipeListMap.get(mod)));
		return true;
	}

	static class RecipeModListElement extends SimpleListElement
	{
		RecipeModListElement (String title)
		{
			super(title);
		}
		public boolean click(int button)
		{
			if (button == 0)
				return GuiRecipeList.showModRecipeList(this.title);
			return false;
		}
	}
}