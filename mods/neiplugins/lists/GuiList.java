package mods.neiplugins.lists;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemStack;

import cpw.mods.fml.common.Mod;

import codechicken.nei.GuiNEIButton;
import codechicken.nei.NEIClientConfig;
import codechicken.nei.NEIClientConfig;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.api.IGuiContainerOverlay;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.ContainerRecipe;

public class GuiList extends GuiContainer implements IGuiContainerOverlay
{
	protected GuiList(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(new ContainerRecipe());
		slotcontainer = (ContainerRecipe) inventorySlots;

		this.prevGui = prevgui;
		this.firstGui = prevgui;
		this.title = title;
		this.elements = list;
		this.elementSize = new Dimension(getElementWidth(), getElementHeight());
		this.page = 0;

		if(prevgui instanceof IGuiContainerOverlay)
		{
			this.firstGui = ((IGuiContainerOverlay)prevgui).getFirstScreen();
		}
	}

	public static boolean showList(String title, ArrayList<? extends IListElement> list)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiList(prevscreen, title, list));
		return true;
	}

	@Override
	public boolean isClientOnly()
	{
		return true;
	}

	public void initGui()
	{
		super.initGui();

		nextpage = new GuiNEIButton(0, width/2 - 70, (height+ySize)/2 - 18, 13, 12, "<");
		prevpage = new GuiNEIButton(1, width/2 + 57, (height+ySize)/2 - 18, 13, 12, ">");
		buttonList.add(nextpage);
		buttonList.add(prevpage);
		if (elements.size() <= elementsPerPage())
		{
			nextpage.drawButton = false;
			prevpage.drawButton = false;
		}

                if (page > (elements.size()-1)/elementsPerPage())
                    page = (elements.size()-1)/elementsPerPage();

		if (page < 0)
                    page = 0;

		refreshPage();
	}
	@Override
	public void keyTyped(char c, int i)
	{
		if(i == 1)//esc
		{
			firstGui.refresh();
			mc.displayGuiScreen(firstGui);
			return;
		}
		if(manager.lastKeyTyped(i, c))
		{
			return;
		}
		/*IRecipeHandler recipehandler = currenthandlers.get(recipetype);
		for(int recipe = page * recipehandler.recipiesPerPage(); recipe < recipehandler.numRecipes() && recipe < (page + 1) * recipehandler.recipiesPerPage(); recipe++)
		{
			if(recipehandler.keyTyped(this, c, i, recipe))
				return;
		}*/
		if(i == mc.gameSettings.keyBindInventory.keyCode)
		{
			firstGui.refresh();
			mc.displayGuiScreen(firstGui);
		}
		else if(i == NEIClientConfig.getKeyBinding("back"))
		{
			prevGui.refresh();
			mc.displayGuiScreen(prevGui);
		}
	}

	public int getHoveredPosition() {
		Point mousepos = manager.getMousePosition();
		Point relMouse = new Point(mousepos.x - this.guiLeft, mousepos.y - this.guiTop);
		int pos = (relMouse.y-16)/getElementsOffset();
		if (getElementRectangle(pos).contains(relMouse))
			return pos;
		return -1;
	}

	public int getHoveredIndex() {
		Point mousepos = manager.getMousePosition();
		Point relMouse = new Point(mousepos.x - this.guiLeft, mousepos.y - this.guiTop);
		int pos = (relMouse.y-16)/getElementsOffset();
		int index = pos + page*elementsPerPage();
		if (index >= 0 && index < elements.size() && getElementRectangle(pos).contains(relMouse))
			return index;
		return -1;
	}

	@Override
	public List<String> handleTooltip(int mousex, int mousey, List<String> currenttip)
	{
		Point mousepos = manager.getMousePosition();
		Point relMouse = new Point(mousepos.x - this.guiLeft, mousepos.y - this.guiTop);
		int pos = (relMouse.y-16)/getElementsOffset();
		int index = pos + page*elementsPerPage();
		if (index < 0 || index >= elements.size() || !getElementRectangle(pos).contains(relMouse))
			return currenttip;

		currenttip = elements.get(index).handleTooltip(this, currenttip, new Point(relMouse.x-5-getLeftOffset(),(relMouse.y-16)%getElementsOffset()));

		return currenttip;
	}

	@Override
	public List<String> handleItemTooltip(ItemStack stack, int mousex, int mousey, List<String> currenttip)
	{
		Point mousepos = manager.getMousePosition();
		Point relMouse = new Point(mousepos.x - this.guiLeft, mousepos.y - this.guiTop);
		int pos = (relMouse.y-16)/getElementsOffset();
		int index = pos + page*elementsPerPage();
		if (index < 0 || index >= elements.size() || !getElementRectangle(pos).contains(relMouse))
			return currenttip;

		currenttip = elements.get(index).handleItemTooltip(this, stack, currenttip, new Point(relMouse.x-5-getLeftOffset(),(relMouse.y-16)%getElementsOffset()));

		return currenttip;
	}

	@Override
	protected void mouseClicked(int par1, int par2, int par3)
	{
		int index = getHoveredIndex();
		if (index != -1) {
			if (elements.get(index).click(par3))
				return;
		}

		super.mouseClicked(par1, par2, par3);
	}

	protected void actionPerformed(GuiButton guibutton)
	{
		super.actionPerformed(guibutton);
		switch(guibutton.id)
		{
			case 0:
				prevPage();
			break;
			case 1:
				nextPage();
			break;
		}
	}
	public void onUpdate()
	{
        	if(!NEIClientUtils.shiftKey())
	            cycleticks++;
	}
	public void updateScreen()
	{
		super.updateScreen();
		onUpdate();
		refreshPage();
	}

	private void nextPage()
	{
		page++;
		if(page > ((elements.size()-1) / elementsPerPage()))
		{
			page = 0;
		}
	}

	private void prevPage()
	{
		page--;
		if(page < 0 )
		{
			page = (elements.size()-1) / elementsPerPage();
		}
	}

	public void refreshPage()
	{
		refreshSlots();
	}

	private void refreshSlots()
	{
		slotcontainer.inventorySlots.clear();
		for(int i = page * elementsPerPage(); i < (page + 1) * elementsPerPage() && i < elements.size(); i++)
		{
			Point p = getElementPosition(i);
			ArrayList<PositionedStack> stacks = getPositionedStacks(i);
			if (stacks != null)
			{
				for(PositionedStack stack : stacks)
				{
					slotcontainer.addSlot(stack, p.x, p.y);
				}
			}
		}

	}
	public ArrayList<PositionedStack> getPositionedStacks(int index)
	{
		return getCycledIngredients(cycleticks/20, elements.get(index).getStacks());
	}

        public ArrayList<PositionedStack> getCycledIngredients(int cycle, ArrayList<PositionedStack> ingredients)
        {
		if (ingredients == null) return null;
		for(int itemIndex = 0; itemIndex < ingredients.size(); itemIndex++)
		{
			//this is so that all the items are shuffled if there is more than 1 in the dictionary.
			PositionedStack stack = ingredients.get(itemIndex);
			Random rand = new Random(cycle+itemIndex);
			stack.setPermutationToRender(Math.abs(rand.nextInt())%stack.items.length);
		}
            return ingredients;
        }

	protected void drawForeground(int index, boolean empty, boolean hovered)
	{
		if (!empty)
			elements.get(index).draw(manager, this.elementSize);
	}

	protected void drawBackground(int index, boolean empty, boolean hovered)
	{
		if (!empty) {
			mc.renderEngine.bindTexture("/gui/gui.png");
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			drawTexturedModalRect(0, 0, 0, 46 + (isActive(index)?(hovered?40:20):0), getElementWidth() / 2, getElementHeight());
			drawTexturedModalRect(0 + getElementWidth() / 2, 0, 200 - getElementWidth() / 2, 46 + (isActive(index)?(hovered?40:20):0), getElementWidth() / 2, getElementHeight());
		}
	}

	protected boolean isActive(int index)
	{
		if (elements.get(index) instanceof IActiveListElement)
			return ((IActiveListElement)elements.get(index)).isActive();
		return true;
	}

	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
		int pos = getHoveredPosition();

		GuiContainerManager.enable2DRender();

		fontRenderer.drawString(this.title, (xSize - fontRenderer.getStringWidth(this.title)) / 2, 5, 0x404040);

		String s = "Use \""+Keyboard.getKeyName(NEIClientConfig.getKeyBinding("back"))+"\" key to go back";
		fontRenderer.drawString(s, (xSize - fontRenderer.getStringWidth(s)) / 2, ySize - 16 - 13, 0x404040);

		s = "Page "+(page+1)+"/"+((elements.size()-1) / elementsPerPage() + 1);
		fontRenderer.drawString(s, (xSize - fontRenderer.getStringWidth(s)) / 2, ySize - 16, 0x404040);


		GL11.glPushMatrix();
		GL11.glTranslatef(5+getLeftOffset(), 16, 0);
		for(int i = page * elementsPerPage(); i < (page + 1) * elementsPerPage(); i++)
		{
			drawForeground(i, i >= elements.size(), i%elementsPerPage() == pos);
			GL11.glTranslatef(0, getElementsOffset(), 0);
		}

		GL11.glPopMatrix();
	}

	protected void drawGuiContainerBackgroundLayer(float f, int mx, int my)
	{
		int pos = getHoveredPosition();

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture("/codechicken/nei/recipebg.png");
		int j = (width - xSize) / 2;
		int k = (height - ySize) / 2;

		drawTexturedModalRect(j, k, 0, 0, xSize, ySize);

		GL11.glPushMatrix();
		GL11.glTranslatef(j+5+getLeftOffset(), k+16, 0);
		for(int i = page * elementsPerPage(); i < (page + 1) * elementsPerPage(); i++)
		{
			drawBackground(i, i >= elements.size(), i%elementsPerPage() == pos);
			GL11.glTranslatef(0, getElementsOffset(), 0);
		}
		GL11.glPopMatrix();
	}

	@Override
	public GuiContainer getFirstScreen()
	{
		return firstGui;
	}

	public int getElementsOffset()
	{
		return 23;
	}

	public int getLeftOffset()
	{
		if (getElementWidth()>166)
			return 0;
		return (166-getElementWidth())/2;
	}

        public int getElementHeight()
        {
		return 20;
        }
	public int getElementWidth()
	{
	    return 140;
	}
	public Point getElementPosition(int pos)
	{
		return new Point(5+getLeftOffset(), 16 + (pos % elementsPerPage()) * getElementsOffset());
	}

	public Rectangle getElementRectangle(int pos)
	{
		return new Rectangle(getElementPosition(pos), this.elementSize);
	}

	public int elementsPerPage()
	{
		return (130) / getElementsOffset();
	}

	public int page;
	public ContainerRecipe slotcontainer;
	public GuiContainer firstGui;
	public GuiContainer prevGui;
	public GuiButton nextpage;
	public GuiButton prevpage;
	public String title;
	public Dimension elementSize;

	public ArrayList<? extends IListElement> elements = new ArrayList<IListElement>();
	public int cycleticks = Math.abs((int)System.currentTimeMillis());
}