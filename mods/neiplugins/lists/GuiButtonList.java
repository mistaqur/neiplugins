package mods.neiplugins.lists;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.NEIClientUtils;

public class GuiButtonList extends GuiList
{
	protected GuiButtonList(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}


	public static boolean showList(String title, ArrayList<? extends IListElement> list)
	{
		if(list.isEmpty())
			return false;

		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		NEIClientUtils.overlayScreen(new GuiButtonList(prevscreen, title, list));
		return true;
	}
	@Override
	protected void drawForeground(int index, boolean empty, boolean hovered)
	{
		if (!empty)
			elements.get(index).draw(manager, this.elementSize);
	}

	@Override
	protected void drawBackground(int index, boolean empty, boolean hovered)
	{
		if (!empty) {
			mc.renderEngine.bindTexture("/gui/gui.png");
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			drawTexturedModalRect(0, 0, 0, 46 + (isActive(index)?(hovered?40:20):0), getElementWidth() / 2, getElementHeight());
			drawTexturedModalRect(0 + getElementWidth() / 2, 0, 200 - getElementWidth() / 2, 46 + (isActive(index)?(hovered?40:20):0), getElementWidth() / 2, getElementHeight());
		}
	}

	@Override
	public int getElementsOffset()
	{
		return 23;
	}

	@Override
        public int getElementHeight()
        {
		return 20;
        }

	@Override
	public int getElementWidth()
	{
	    return 140;
	}
}