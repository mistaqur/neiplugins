package mods.neiplugins;

import codechicken.nei.forge.GuiContainerManager;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.FuelTooltipHandler;
import mods.neiplugins.common.IPlugin;
import mods.neiplugins.options.OptionsFuelTooltips;

public class NEIPlugins_FuelTooltip implements IPlugin {

	public static final String PLUGIN_NAME = "Fuel Tooltip";
	public static final String PLUGIN_VERSION = "1.0.2";
	public static final String REQUIRED_MOD = "";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void init() {
	}

	@Override
	public void loadConfig() {
		OptionsFuelTooltips.addOptions();
		GuiContainerManager.addTooltipHandler(new FuelTooltipHandler());
	}

}