package mods.neiplugins;

import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.RecipeHandlerUtils;
import mods.neiplugins.te.TEFuelHelper;

public class NEIPlugins_TE implements IPlugin {

	public static final String PLUGIN_NAME = "ThermalExpansion";
	public static final String PLUGIN_VERSION = "1.0.1";
	public static final String REQUIRED_MOD = "ThermalExpansion@[2.3.1.0,)";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		return NEIPlugins.getMod().hasMod(REQUIRED_MOD);
	}

	@Override
	public void init() {
	}

	@Override
	public void loadConfig() {
		RecipeHandlerUtils.addToRecipeList("Thermal Expansion","Pulverizer",0,"thermalexpansion.pulverizer");
		RecipeHandlerUtils.addToRecipeList("Thermal Expansion","Induction Smelter",0,"thermalexpansion.smelter");
		RecipeHandlerUtils.addToRecipeList("Thermal Expansion","Sawmill",0,"thermalexpansion.sawmill");
		RecipeHandlerUtils.addToRecipeList("Thermal Expansion","Magma Crucible",0,"thermalexpansion.crucible");
		RecipeHandlerUtils.addToRecipeList("Thermal Expansion","Liquid Transposer",0,"thermalexpansion.transposer");
		RecipeHandlerUtils.addToRecipeList("Thermal Expansion","Powered Furnace",0,"thermalexpansion.furnace");

		TEFuelHelper.registerFuelHelpers();
	}

}