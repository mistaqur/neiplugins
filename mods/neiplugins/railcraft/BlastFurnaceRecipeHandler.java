package mods.neiplugins.railcraft;

import java.awt.Rectangle;
import java.util.ArrayList;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.FurnaceRecipeHandler;

import mods.railcraft.api.crafting.IBlastFurnaceRecipe;
import mods.railcraft.api.crafting.RailcraftCraftingManager;

public class BlastFurnaceRecipeHandler extends FurnaceRecipeHandler
{
	public class CachedBlastRecipe extends CachedRecipe
	{
		IBlastFurnaceRecipe funace;
		public CachedBlastRecipe(IBlastFurnaceRecipe base)
		{
			funace = base;
		}

		@Override
		public PositionedStack getResult()
		{
			return new PositionedStack(funace.getOutput(), 111, 24);
		}

		@Override
		public ArrayList<PositionedStack> getIngredients()
		{
			ArrayList<PositionedStack> stacks = new ArrayList<PositionedStack>();
			stacks.add(new PositionedStack(funace.getInput(), 51, 6));
			return getCycledIngredients(cycleticks / 20, stacks);
		}

		@Override
		public PositionedStack getOtherStack()
		{
			if (afuels.length>0)
				return new PositionedStack(afuels[cycleticks/48 % afuels.length], 51, 42);
			return null;
		}
	}

	static final ItemStack[] afuels = RailcraftCraftingManager.blastFurnace.getFuels().toArray(new ItemStack[0]);

	@Override
	public String getRecipeName()
	{
		return "Blast Furnace";
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(74, 23, 24, 18), "railcraft.blastfurnace"));
	}

	public static Class<? extends GuiContainer> guiclass;

	@Override
	public Class<? extends GuiContainer> getGuiClass()
	{
		return guiclass;
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals("railcraft.blastfurnace") && getClass() == BlastFurnaceRecipeHandler.class)
		{
			for(IBlastFurnaceRecipe recipe : RailcraftCraftingManager.blastFurnace.getRecipes())
			{
				arecipes.add(new CachedBlastRecipe(recipe));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	public void loadUsageRecipes(ItemStack ingred)
	{

		for(IBlastFurnaceRecipe recipe : RailcraftCraftingManager.blastFurnace.getRecipes())
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(recipe.getInput(), ingred))
			{
				arecipes.add(new CachedBlastRecipe(recipe));
			}
		}
	}

	public void loadCraftingRecipes(ItemStack result)
	{

		for(IBlastFurnaceRecipe recipe : RailcraftCraftingManager.blastFurnace.getRecipes())
		{
			if(NEIClientUtils.areStacksSameType(result, recipe.getOutput()))
			{
				arecipes.add(new CachedBlastRecipe(recipe));
			}
		}
	}

	@Override
	public String getGuiTexture()
	{
		return "/gui/furnace.png";
	}

	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		drawProgressBar(gui, 51, 25, 176, 0, 14, 14, 48, 7);
		drawProgressBar(gui, 74, 23, 176, 14, 24, 16, 48, 0);
	}
}