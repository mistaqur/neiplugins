package mods.neiplugins.railcraft;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler;

import mods.railcraft.api.crafting.IRockCrusherRecipe;
import mods.railcraft.api.crafting.RailcraftCraftingManager;

public class RockCrusherRecipeHandler extends TemplateRecipeHandler
{
	static final int[][] stackorder = new int[][]{
		{0,0},
		{1,0},
		{2,0},
		{0,1},
		{1,1},
		{2,1},
		{0,2},
		{1,2},
		{2,2}};

	public class CachedRockCrusherRecipe extends CachedRecipe
	{
		public CachedRockCrusherRecipe(IRockCrusherRecipe recipe)
		{

			resources = new ArrayList<PositionedStack>();
			resources.add(new PositionedStack(recipe.getInput(), 3, 10));
			products = new PositionedStack[9];
			chances = new float[9];
			for (int i=0; i < 9; i++)
				chances[i] = 0;
			setProducts(recipe.getOutputs());
		}

		public float getProductChance(PositionedStack stack)
		{
			for (int i=0; i<9; i++)
			{
				if (products[i] == stack)
					return chances[i];
			}
			return 0.0F;
		}

		public void setProducts(List<Entry<ItemStack, Float>> iproducts)
		{
			int i = 0;
			for(Entry<ItemStack, Float> product : iproducts)
			{
				if (i>8)
					break;
				products[i] = new PositionedStack(product.getKey(), 93 + stackorder[i][0] * 18, 10 + stackorder[i][1] * 18);
				chances[i] = product.getValue();
				i++;
			}
		}

		@Override
		public ArrayList<PositionedStack> getOtherStacks()
		{
			ArrayList<PositionedStack> stacks = new ArrayList<PositionedStack>();
			for(int i=1; i < 9; i++)
			{
				if (products[i] != null)
					stacks.add(products[i]);
			}
			return stacks;
		}


		@Override
		public ArrayList<PositionedStack> getIngredients()
		{
			return getCycledIngredients(cycleticks / 20, resources);
		}

		public boolean canProduce(ItemStack product)
		{
			for (int i=0; i<9; i++)
			{
				if(products[i] != null && NEIClientUtils.areStacksSameTypeCrafting(products[i].item, product))
				{
					return true;
				}
			}
			return false;
		}

		@Override
		public PositionedStack getResult()
		{
			return products[0];
		}

		PositionedStack[] products;
		float[] chances;
		ArrayList<PositionedStack> resources;
	}

	@Override
	public String getRecipeName()
	{
		return "Rock Crusher";
	}

	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		drawProgressBar(gui, 59, 9, 176, 0, 29, 53, 48, 0);
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(59, 9, 29, 53), "railcraft.crusher"));
	}

	public static Class<? extends GuiContainer> guiclass;

	@Override
	public Class<? extends GuiContainer> getGuiClass()
	{
		return guiclass;
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals("railcraft.crusher") && getClass() == RockCrusherRecipeHandler.class)
		{
			for (IRockCrusherRecipe irecipe : RailcraftCraftingManager.rockCrusher.getRecipes())
			{
				arecipes.add(new CachedRockCrusherRecipe(irecipe));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		for (IRockCrusherRecipe irecipe : RailcraftCraftingManager.rockCrusher.getRecipes())
		{
			CachedRockCrusherRecipe recipe = new CachedRockCrusherRecipe(irecipe);
			if(recipe.canProduce(result))
			//if (irecipe.getOutputs().containsKey(result))
			{
				//CachedRockCrusherRecipe recipe = new CachedRockCrusherRecipe(irecipe);
				arecipes.add(recipe);
			}
		}

	}
	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		for (IRockCrusherRecipe irecipe : RailcraftCraftingManager.rockCrusher.getRecipes())
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(irecipe.getInput(), ingredient))
			{
				if (irecipe.getOutputs().size()>0)
				{
					arecipes.add(new CachedRockCrusherRecipe(irecipe));
				}
			}
		}
	}

	public Rectangle getRectangleFromPositionedStack(PositionedStack stack)
	{
		return new Rectangle(stack.relx-1, stack.rely-1, 18, 18);
	}

	@Override
	public List<String> handleItemTooltip(GuiRecipe guiRecipe, ItemStack stack, List<String> currenttip, int recipe)
	{
		CachedRockCrusherRecipe crecipe = (CachedRockCrusherRecipe)arecipes.get(recipe);
		Point mousepos = guiRecipe.manager.getMousePosition();
		Point offset = guiRecipe.getRecipePosition(recipe);
		Point relMouse = new Point(mousepos.x - guiRecipe.guiLeft - offset.x, mousepos.y - guiRecipe.guiTop - offset.y);
		for (int i=0; i<9; i++)
		{
			if (crecipe.products[i] != null && getRectangleFromPositionedStack(crecipe.products[i]).contains(relMouse))
			{
				float chance = crecipe.chances[i];
				if (chance == 0.0F)
					currenttip.add("Chance: never");
				else if (chance < 0.01)
					currenttip.add("Chance < 1%");
				else if (chance < 1)
					currenttip.add("Chance: "+Integer.toString(Math.round(chance*100))+"%");
			}
		}
		return currenttip;
	}

	@Override
	public boolean hasOverlay(GuiContainer gui, Container container, int recipe)
	{
		return false;
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/railcraft/textures/gui/gui_crusher.png";
	}
}