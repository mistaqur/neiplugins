package mods.neiplugins.railcraft;

import java.awt.Rectangle;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapelessRecipes;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.NEICompatibility;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.RecipeInfo;
import codechicken.nei.recipe.ShapelessRecipeHandler;
import codechicken.nei.recipe.weakDependancy_Forge;

import mods.railcraft.api.crafting.RailcraftCraftingManager;

public class RollingMachineShapelessRecipeHandler extends ShapelessRecipeHandler
{
	@Override
	public String getRecipeName()
	{
		return "Rolling Machine";
	}

	@Override
	public boolean hasOverlay(GuiContainer gui, Container container, int recipe)
	{
		return RecipeInfo.hasDefaultOverlay(gui, getOverlayIdentifier()) || RecipeInfo.hasOverlayHandler(gui, getOverlayIdentifier());
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/railcraft/textures/gui/gui_rolling.png";
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "railcraft.rolling";
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(84, 32, 24, 10), "railcraft.rolling"));
	}

	public static Class<? extends GuiContainer> guiclass;

	@Override
	public Class<? extends GuiContainer> getGuiClass()
	{
		return guiclass;
	}

	public void drawBackground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.bindTexture(getGuiTexture());
		gui.drawTexturedModalRect(0, 0, 5, 11, 150, 65);
	}

	protected CachedShapelessRecipe getCachedRecipe(IRecipe irecipe) {

		CachedShapelessRecipe recipe = null;
		if(irecipe instanceof ShapelessRecipes )
		{
			recipe = new CachedShapelessRecipe((ShapelessRecipes)irecipe);
		}
		else if(NEICompatibility.hasForge && weakDependancy_Forge.recipeInstanceShapeless(irecipe))
		{
			recipe = weakDependancy_Forge.getShapelessRecipe(this, irecipe);
		}
		if (recipe == null)
			return null;
		recipe.result.relx = 88;
		recipe.result.rely = 16;
		return recipe;

	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals("railcraft.rolling"))
		{
			for(IRecipe irecipe : RailcraftCraftingManager.rollingMachine.getRecipeList())
			{
				CachedShapelessRecipe recipe = getCachedRecipe(irecipe);
				if(recipe == null)
					continue;
				arecipes.add(recipe);
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	public void loadUsageRecipes(ItemStack ingredient)
	{
		for(IRecipe irecipe : RailcraftCraftingManager.rollingMachine.getRecipeList())
		{
			CachedShapelessRecipe recipe = getCachedRecipe(irecipe);
			if(recipe == null)
				continue;
			if(recipe.contains(recipe.ingredients, ingredient))
			{
				recipe.setIngredientPermutation(recipe.ingredients, ingredient);
				arecipes.add(recipe);
			}
		}
	}

	public void loadCraftingRecipes(ItemStack result)
	{
		for(IRecipe irecipe : RailcraftCraftingManager.rollingMachine.getRecipeList())
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(irecipe.getRecipeOutput(), result))
			{
				CachedShapelessRecipe recipe = getCachedRecipe(irecipe);
				if(recipe == null)
					continue;
				arecipes.add(recipe);
			}
		}
	}

}