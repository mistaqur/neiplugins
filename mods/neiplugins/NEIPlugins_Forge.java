package mods.neiplugins;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.liquids.ILiquid;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidDictionary;

import codechicken.nei.MultiItemRange;
import codechicken.nei.api.API;
import codechicken.nei.forge.GuiContainerManager;

import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.Registry;
import mods.neiplugins.common.Utils;
import mods.neiplugins.forge.LiquidDictionaryList;
import mods.neiplugins.forge.OreDictionaryList;
import mods.neiplugins.forge.OreDictionaryTooltipHandler;
import mods.neiplugins.options.OptionsForge;

public class NEIPlugins_Forge implements IPlugin {

	public static final String PLUGIN_NAME = "Forge";
	public static final String PLUGIN_VERSION = "1.3.5";
	public static final String REQUIRED_MOD = "Forge";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		return NEIPlugins.getMod().hasMod(REQUIRED_MOD);
	}
	@Override
	public void init() {
	}

	@Override
	public void loadConfig() {
		addLiquidContainerSubset();
		API.registerUsageHandler(new OreDictionaryList());
		API.registerRecipeHandler(new OreDictionaryList());
		API.registerUsageHandler(new LiquidDictionaryList());
		API.registerRecipeHandler(new LiquidDictionaryList());
		OreDictionaryTooltipHandler.resetCache();
		GuiContainerManager.addTooltipHandler(new OreDictionaryTooltipHandler());

		MultiItemRange liquids = new MultiItemRange();

		for (Block i:Block.blocksList)
		{
			if (i instanceof ILiquid)
			{
				liquids.add(i.blockID);
			}
		}
		API.addSetRange("Blocks.Liquids", liquids);

		OptionsForge.addOptions();
	}

	private void addLiquidContainerSubset() {

		Set<List> setContainerValidation = new HashSet();
		Map<List, MultiItemRange> mapMultiItemRangeFromLiquid = new HashMap();
		MultiItemRange containers = new MultiItemRange();

		for (LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData())
		{
			if (!setContainerValidation.contains(Arrays.asList(data.container.itemID, data.container.getItemDamage()))) {
				setContainerValidation.add(Arrays.asList(data.container.itemID, data.container.getItemDamage()));
				containers.add(data.container);
			}
			if (LiquidHelper.isSafeLiquidStack(data.stillLiquid,"NEIPlugins_Forge.addLiquidContainerSubset() for filled liquid container "+Utils.getItemStackDebug(data.filled)))
			{
				MultiItemRange range = mapMultiItemRangeFromLiquid.get(Arrays.asList(data.stillLiquid.itemID, data.stillLiquid.itemMeta));
				if (range == null) {
					String liquidName = LiquidHelper.getLiquidName(data.stillLiquid);
					range = new MultiItemRange();
					mapMultiItemRangeFromLiquid.put(Arrays.asList(data.stillLiquid.itemID, data.stillLiquid.itemMeta), range);
					if (!liquidName.isEmpty())
						API.addSetRange("Items.Liquid Containers."+liquidName, range);
				}
				range.add(data.filled);
			}
		}
		API.addSetRange("Items.Liquid Containers", containers);
	}
}