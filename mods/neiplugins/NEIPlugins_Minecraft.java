package mods.neiplugins;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.RecipeHandlerUtils;

public class NEIPlugins_Minecraft implements IPlugin
{
	public static final String PLUGIN_NAME = "Minecraft";
	public static final String PLUGIN_VERSION = "1.0.0";
	public static final String REQUIRED_MOD = "";

	@Override
	public String getPluginName()
	{
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion()
	{
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid()
	{
		return true;
	}

	@Override
	public void init()
	{
	}

	@Override
	public void loadConfig()
	{
		RecipeHandlerUtils.addToRecipeList("Minecraft",new ItemStack(Block.workbench),"Crafting",0,"crafting");
		RecipeHandlerUtils.addToRecipeList("Minecraft",new ItemStack(Block.furnaceIdle),"Furnace",0,"smelting");
		RecipeHandlerUtils.addToRecipeList("Minecraft",new ItemStack(Item.brewingStand),"Brewing",0,"brewing");
	}

}