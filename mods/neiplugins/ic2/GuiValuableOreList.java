package mods.neiplugins.ic2;

import java.awt.Dimension;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import codechicken.nei.NEIClientUtils;

import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.IListElement;

public class GuiValuableOreList extends GuiList
{
	protected GuiValuableOreList(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}

	@Override
	public int getElementsOffset()
	{
		return 20;
	}

	@Override
        public int getElementHeight()
        {
		return 20;
        }

	@Override
	protected void drawBackground(int index, boolean empty, boolean hovered)
	{
	}

	public static boolean showList(String title)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		ArrayList<Entry<ItemStack,Integer>> items = new ArrayList<Entry<ItemStack,Integer>>();

		Dimension eSize = new Dimension(140,16);
		for (Integer blockID : new TreeSet<Integer>(valuableOres.keySet()))
		{
			Map<Integer,Integer> metavalues = valuableOres.get(blockID);
			for (Integer blockMeta : new TreeSet<Integer>(metavalues.keySet()))
			{
				ItemStack stack = new ItemStack(blockID.intValue(), 1, blockMeta.intValue());
				if (stack.getItem()!=null) {
					items.add(new SimpleEntry<ItemStack,Integer>(stack, metavalues.get(blockMeta)));
				}
			}
		}

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		Iterator<Entry<ItemStack,Integer>> stacks = items.iterator();

		while (stacks.hasNext())
		{
			list.add(new ValuableOreListElement(stacks, 4, eSize));
		}

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiValuableOreList(prevscreen, title, list));
		return true;
	}
	public static Map<Integer,Map<Integer,Integer>> valuableOres;
}