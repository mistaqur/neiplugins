package mods.neiplugins.ic2;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import codechicken.nei.NEIClientUtils;

import ic2.api.recipe.Recipes;
import mods.neiplugins.common.ItemStackComparator;
import mods.neiplugins.common.Utils;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.IListElement;
import mods.neiplugins.lists.MultipleStackListElement;

public class GuiRecyclerBlacklist extends GuiList
{
	protected GuiRecyclerBlacklist(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}

	@Override
	public int getElementsOffset()
	{
		return 20;
	}

	@Override
        public int getElementHeight()
        {
		return 16;
        }

	@Override
	protected void drawBackground(int index, boolean empty, boolean hovered)
	{
	}

	public static boolean showList(String title)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		Iterator<ItemStack> stacks = getRecyclerBlacklist().iterator();
		Dimension eSize = new Dimension(140,16);
		while (stacks.hasNext())
		{
			list.add(new MultipleStackListElement(stacks, 4, eSize));
		}

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiRecyclerBlacklist(prevscreen, title, list));
		return true;
	}

	public static List<ItemStack> getRecyclerBlacklist()
	{
		List<ItemStack> lst = new ArrayList<ItemStack>();
		lst.addAll(Recipes.recyclerBlacklist.getStacks());
		Collections.sort(lst, new ItemStackComparator());
		return lst;
	}
}