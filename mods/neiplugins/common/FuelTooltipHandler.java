package mods.neiplugins.common;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import codechicken.nei.forge.IContainerTooltipHandler;

import mods.neiplugins.common.TooltipCache;

public class FuelTooltipHandler implements IContainerTooltipHandler
{
	public static TooltipCache tooltipCache = new TooltipCache();

	@Override
	public List<String> handleTooltipFirst(GuiContainer gui, int mousex, int mousey, List<String> currenttip)
	{
		return currenttip;
	}

	@Override
	public List<String> handleItemTooltip(GuiContainer gui, ItemStack stack, List<String> currenttip) {
		if (stack == null)
			return currenttip;

		boolean displayFuelTooltip = true;

		if (FuelTooltip.showContextTooltip && gui != null)
			for (IFuelContextHelper helper : FuelTooltip.contextHelpers.values())
				if (!FuelTooltip.disabledContextHelpers.contains(helper.getKey()) && helper.haveContextTooltip(gui)) {
					currenttip = helper.getContextTooltip(gui, stack, currenttip);
					if (!helper.displayFuelTooltip(gui))
						displayFuelTooltip = false;
				}

		if (!displayFuelTooltip)
			return currenttip;

		if (tooltipCache.addCachedTooltip(stack, currenttip) || !FuelTooltip.isValidFuelItem(stack))
			return currenttip;

		List<String> fueltooltip = new ArrayList<String>();
		FuelTooltip.addCommonFuelTooltip(fueltooltip, stack, FuelTooltip.disabledHelpers);

		if (fueltooltip.size()>0) {
			fueltooltip.add(0,"\u00a77Can produce:");
			currenttip.addAll(fueltooltip);
		}

		if (!stack.hasTagCompound())
		{
			if (fueltooltip.size()>0)
				tooltipCache.put(stack, fueltooltip);
			else
				tooltipCache.put(stack, null);
		}

		return currenttip;
	}

}