package mods.neiplugins.common;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import mods.neiplugins.lists.RecipeHandlerListElement;

public abstract class RecipeHandlerUtils {
	public static Map<String,ArrayList<RecipeHandlerListElement>> recipeListMap = new TreeMap<String,ArrayList<RecipeHandlerListElement>>();

	public static boolean addToRecipeList(String mod, String recipeType, int type, String id, Object... params)
	{
		return addToRecipeList(mod, null, recipeType, type, id, params);
	}

	public static boolean addToRecipeList(String mod, ItemStack icon, String recipeType, int type, String id, Object... params)
	{
		if (!recipeListMap.containsKey(mod))
			recipeListMap.put(mod,new ArrayList<RecipeHandlerListElement>());

		for (RecipeHandlerListElement element : recipeListMap.get(mod))
		{
			if (element.id.equals(id))
				return false;
		}

		recipeListMap.get(mod).add(new RecipeHandlerListElement(recipeType, icon, type, id, params));
		return true;
	}
}