package mods.neiplugins.common;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Level;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import codechicken.core.config.ConfigTag;
import codechicken.nei.NEIClientConfig;
import codechicken.nei.NEIServerUtils;

import mods.neiplugins.NEIPlugins;

public abstract class Utils {
	public static class MethodInvoker {
		private static Object obj;
		private static Method method;
		public MethodInvoker(Method method, Object obj)
		{
			this.method = method;
			this.obj = obj;
		}
		public Object invoke(Object... args)
		{
			try {
				return this.method.invoke(this.obj, args);
			} catch (Exception ex) {
				NEIPlugins.log(Level.INFO,"Failed to invoke method \"{1}\" from class \"{0}\"", new Object[]{method.getDeclaringClass().getName(),method.getName()});
			}
			return null;
		}
	}

	public static Class findClass(String clsname)
	{
		try {
			return Class.forName(clsname);
		} catch (ClassNotFoundException ex) {
			NEIPlugins.log(Level.FINE,"Failed to get class \"{0}\"", new Object[]{clsname});
		}
		return null;
	}

	public static Method findMethod(Class cls, String name, Class... types)
	{
		try {
			return cls.getDeclaredMethod(name, types);
		} catch (NoSuchMethodException ex) {
			NEIPlugins.log(Level.FINE,"Failed to get method \"{1}\" from class \"{0}\"", new Object[]{cls.getName(), name});
		}
		return null;
	}
	public static MethodInvoker getMethodInvoker(Class cls, String name, Object instance, Class... types)
	{
		try {
			Method method = cls.getDeclaredMethod(name, types);
			return new MethodInvoker(method, instance);
		} catch (NoSuchMethodException ex) {
			NEIPlugins.log(Level.FINE,"Failed to get method \"{1}\" from class \"{0}\"", new Object[]{cls.getName(), name});
		}
		return null;
	}

	public static Object getField(Class cls, String fieldName, Object instance)
	{
		try {
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(instance);
		} catch (NoSuchFieldException ex) {
			NEIPlugins.logWarning("Failed to get field \"{1}\" from class \"{0}\"", cls.getName(), fieldName);
		} catch (Exception ex) {
			NEIPlugins.logWarningEx("Failed to get field \"{1}\" from class \"{0}\": {2}", ex, cls.getName(), fieldName);
		}
		return null;
	}

	public static int getFieldInt(Class cls, String fieldName, Object instance)
	{
		try {
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.getInt(instance);
		} catch (NoSuchFieldException ex) {
			NEIPlugins.logWarning("Failed to get int field \"{1}\" from class \"{0}\"", cls.getName(), fieldName);
		} catch (Exception ex) {
			NEIPlugins.logWarningEx("Failed to get int field \"{1}\" from class \"{0}\": {2}", ex, cls.getName(), fieldName);
		}
		return 0;
	}

	public static int getFieldInt(Class cls, int fieldIndex, Object instance)
	{
		try {
			Field field = cls.getDeclaredFields()[fieldIndex];
			field.setAccessible(true);
			return field.getInt(instance);
		} catch (Exception ex) {
			NEIPlugins.logWarningEx("Failed to get int field \"{1}\" from class \"{0}\": {2}", ex, cls.getName(), fieldIndex);
		}
		return 0;
	}

	public static boolean isSafeItemStack(ItemStack stack)
	{
		if (stack == null || stack.getItem() == null)
			return false;
		return true;
	}

	public static boolean isSafeItemStack(ItemStack stack, String source)
	{
		if (stack == null)
			return false;
		if (stack.getItem() == null) {
			try {
				NEIPlugins.log(Level.WARNING,"Detected invalid ItemStack \"{0}\" in \"{1}\"", stack.toString(), source);
			}
			catch (Exception ex)
			{
				NEIPlugins.log(Level.WARNING,"Detected invalid ItemStack {0}:{1} in \"{2}\"", stack.itemID, getFieldInt(ItemStack.class, "itemDamage", stack), source);
			}
			return false;
		}
		return true;
	}


	public static ItemStack getItemStackByFieldName(Class cls, Object instance, String fieldName)
	{
		Object result = null;
		try
		{
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);
			result = field.get(instance);
		}
		catch (NoSuchFieldException e)
		{
			NEIPlugins.log(Level.FINE,"Failed to get field \"{1}\" from class \"{0}\"", new Object[] {cls.getCanonicalName(), fieldName});
			return null;
		}
		catch (Exception e)
		{
			NEIPlugins.log(Level.FINE,"Failed to get field \"{1}\" from class \"{0}\": {2}", new Object[] {cls.getCanonicalName(), fieldName, e.getMessage()});
			return null;
		}
		if (result == null)
			return null;

		if (result instanceof Item)
			return new ItemStack((Item)result);
		else if (result instanceof Block)
			return new ItemStack((Block)result);
		else if (result instanceof ItemStack)
			return (ItemStack)result;
		else
			NEIPlugins.log(Level.FINE,"Unknown class type for field \"{1}\" from class \"{0}\": {2}", new Object[] {cls.getCanonicalName(), fieldName, result.getClass().getCanonicalName()});
		return null;
	}

	public static String getItemStackDebug(ItemStack stack)
	{
		if (stack == null)
			return "null";
		return stack.toString() + " ("+stack.itemID+":"+getFieldInt(ItemStack.class, 2, stack)+")";
	}

	public static int itemStackToInt(ItemStack aStack) {
		if (aStack == null) return 0;
		return aStack.itemID | (aStack.getItemDamage()<<16);
	}

	public static int itemStackWildcardToInt(ItemStack aStack) {
		if (aStack == null) return 0;
		return aStack.itemID | (OreDictionary.WILDCARD_VALUE << 16);
	}
	public static String logFormat(String msg, Object... params) {
		String m = msg;

		for (int i = 0; i < params.length; i++) {
			String t = "NULL";
			if (params[i] instanceof ItemStack)
				t = getItemStackDebug((ItemStack)params[i]);
			else if (params[i] != null)
				t = params[i].toString();
			m = m.replace("{" + i + "}", t);
		}
		return m;

	}

	public static boolean areStacksSameTypeCrafting(ItemStack recipeStack, ItemStack stack)
	{
		return NEIServerUtils.areStacksSameTypeCrafting(recipeStack, stack);
	}

	public static boolean getBooleanSetting(String name) {
		ConfigTag v = NEIClientConfig.getSetting(name);
		if (v.getValue() == null) {
			NEIPlugins.logWarning("Setting \"{0}\" is null", name);
			v.setBooleanValue(false);
		}
		return v.getBooleanValue();
	}

}