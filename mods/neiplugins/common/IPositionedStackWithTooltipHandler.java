package mods.neiplugins.common;

import java.util.List;

import codechicken.nei.recipe.GuiRecipe;

public interface IPositionedStackWithTooltipHandler
{
	public List<String> handleTooltip(GuiRecipe guiRecipe, List<String> currenttip);
}