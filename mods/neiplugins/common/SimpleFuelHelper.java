package mods.neiplugins.common;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import mods.neiplugins.common.IFuelHelper;

public class SimpleFuelHelper implements IFuelHelper
{
	protected String key;
	protected String name;
	protected List<String> desc;

	public SimpleFuelHelper(String key, String name, List<String> desc)
	{
		this.key = key;
		this.name = name;
		this.desc = desc;
	}
	@Override
	public String getName()
	{
		return this.name;
	}
	@Override
	public String getKey()
	{
		return this.key;
	}
	@Override
	public List<String> getDescription()
	{
		return this.desc;
	}

	@Override
	public List<String> getLiquidStackFuelTooltip(LiquidStack liquid, List<String> currenttip)
	{
		return currenttip;
	}

	@Override
	public List<String> getItemStackFuelTooltip(ItemStack stack, List<String> currenttip)
	{
		return currenttip;
	}

}