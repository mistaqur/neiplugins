package mods.neiplugins.common;

import java.awt.Point;
import java.awt.Rectangle;
import java.text.DecimalFormat;
import java.util.List;

import net.minecraft.util.Icon;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.forge.GuiContainerManager;

public class LiquidTank
{
	public LiquidStack liquid;
	public Rectangle position;
	public Point overlay;
	public int capacity;
	public boolean showCapacity = true;
	public boolean showAmount = true;
	public boolean showMillBuckets = false;
	public String overrideTitle = null;
	public boolean ignoreCapacity = false;

	static public DecimalFormat liquidAmountFormat = new DecimalFormat("0.000");

	public LiquidTank(LiquidStack liquid, int capacity, Rectangle position, Point overlay)
	{
		if (liquid != null)
			this.liquid = liquid.copy();
		else
			this.liquid = new LiquidStack(0,0);
		this.capacity = capacity;
		this.position = position;
		this.overlay = overlay;
	}

	public LiquidTank(LiquidStack liquid, int capacity, Rectangle position)
	{
		this(liquid, capacity, position, null);
	}


	public void draw(GuiContainerManager gui, String guiTexture)
	{
		drawLiquidTank(gui, position, liquid, capacity, overlay, guiTexture);
	}
	public String amountToString(int amount)
	{
		if (showMillBuckets)
			return amount + " mB";
		return liquidAmountFormat.format((amount * 1.0) / 1000);
	}

	public List<String> handleTooltip(List<String> currenttip)
	{
		if (liquid.itemID > 0)
		{
			if (this.overrideTitle != null)
				currenttip.add(this.overrideTitle);
			else
				currenttip.add(LiquidHelper.getLiquidName(this.liquid));
			if (showAmount)
				currenttip.add("\u00a77Amount: "+amountToString(liquid.amount));
			if (showCapacity)
				currenttip.add("\u00a77Capacity: "+amountToString(capacity));
			this.additionalHandleTooltip(currenttip);
		}
		else
		{
			currenttip.add("Empty");
		}
		return currenttip;
	}

	public List<String> additionalHandleTooltip(List<String> currenttip){
		return currenttip;
	}

	public void drawLiquidTank(GuiContainerManager gui, Rectangle position, LiquidStack liquid, int capacity, Point overlay, String guiTexture)
	{
		if (liquid == null)
			return;
		int scale = (liquid.amount * position.height) / capacity;
		if (liquid.amount > 0 && ignoreCapacity || scale > position.height)
			scale = position.height;
		Icon liquidIcon = LiquidHelper.bindLiquidTexture(gui, liquid);
		if (liquidIcon == null)
			return;
		//36 0 1 -4
		for (int col = 0; col < position.width / 16; col++) {
			int start = 0;
			int s = scale;
			while (true) {
				int x = 0;
				if (s > 16) {
					x = 16;
					s -= 16;
				} else {
					x = s;
					s = 0;
				}
				gui.window.drawTexturedModelRectFromIcon(position.x + col*16, position.y + position.height - x - start, liquidIcon, 16, 16 - (16 - x));
				start += 16;
				if (x == 0 || s == 0)
					break;
			}
		}
		if (position.width % 16 != 0)
		{
			int start = 0;
			int s = scale;
			while (true) {
				int x = 0;
				if (s > 16) {
					x = 16;
					s -= 16;
				} else {
					x = s;
					s = 0;
				}
				gui.window.drawTexturedModelRectFromIcon(position.x + position.width - (position.width%16), position.y + position.height - x - start, liquidIcon, position.width%16, 16 - (16 - x));
				start += 16;
				if (x == 0 || s == 0)
					break;
			}
		}


		if (overlay != null)
		{
			gui.bindTexture(guiTexture);
			gui.drawTexturedModalRect(position.x, position.y, overlay.x, overlay.y, position.width, position.height);
		}
	}
}