package mods.neiplugins.common;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.logging.Level;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import codechicken.nei.ItemRange;
import codechicken.nei.MultiItemRange;
import codechicken.nei.api.API;

import mods.neiplugins.NEIPlugins;

public class ItemApiHelper {

	public static void hideItem(ItemStack itemstack) {
		if (itemstack != null)
			API.hideItem(itemstack.itemID);
	}

	public static void addItemStackToRange(MultiItemRange range, ItemStack itemstack)
	{
		if (itemstack != null)
			range.add(itemstack.itemID);
	}

	public static void addSetRangeFromItem(String setname, ItemStack stack)
	{
		MultiItemRange t = new MultiItemRange();
		if (stack != null) {
			t.add(stack.itemID);
			API.addSetRange(setname, t);
		}
	}

	public static void addItemToRangeWithNBT(MultiItemRange range, Item item)
	{
		if (item != null) {
			ItemStack stack = new ItemStack(item.itemID, 1, 0);
			range.add(stack.itemID);
			if (stack.hasTagCompound())
				range.addItemIfInRange(stack.itemID, 0, stack.getTagCompound());
		}
	}

	public static void addSetRangeFromItem(String setname, Class cls, String fieldName) {
		MultiItemRange range = new MultiItemRange();
		addByFieldNameToRange(range, cls, fieldName);
		API.addSetRange(setname, range);
	}

	public static void addByFieldNameToRange(MultiItemRange range, Class cls, String fieldName)
	{
		addByFieldNameToRange(range, cls, null, fieldName, false);
	}

	public static void addByFieldNameToRange(MultiItemRange range, Class cls, Object instance, String fieldName, boolean useNBT)
	{
		ItemRange irange = getItemRangeByFieldName(cls, instance, fieldName, useNBT, false);
		if (irange != null)
			range.add(irange);
	}

	public static void setOverrideNameByFieldName(String itemName, Class cls, String fieldName) {
		setOverrideNameByFieldName(itemName, cls, null, fieldName, false);
	}

	public static void setOverrideNameByFieldName(String itemName, Class cls, Object instance, String fieldName, boolean force) {
		ItemRange irange = getItemRangeByFieldName(cls, instance, fieldName, false, true);
		if (irange != null) {
			ItemStack item = new ItemStack(irange.firstID, 1, irange.firstDamage);
			if (item.getDisplayName().isEmpty() || force)
				API.setOverrideName(irange.firstID, irange.firstDamage, itemName);
		}
	}

	public static void hideItemByFieldName(Class cls, String fieldName)
	{
		hideItemByFieldName(cls, null, fieldName);
	}

	public static void hideItemByFieldName(Class cls, Object instance, String fieldName)
	{
		ItemRange irange = getItemRangeByFieldName(cls, instance, fieldName, false, true);
		if (irange != null)
			API.hideItem(irange.firstID);
	}

	public static ItemRange getSubItemsByItem(Item item)
	{
		if (item == null)
			return null;
		if (item.isDamageable())
			return new ItemRange(item.itemID);

		ArrayList<ItemStack> sublist = new ArrayList<ItemStack>();

		item.getSubItems(item.itemID, null, sublist);

		if (sublist.size() == 0)
			return new ItemRange(item.itemID);

		int itemDmg = sublist.get(0).getItemDamage();
		int dmgMin = itemDmg;
		int dmgMax = itemDmg;


		for (ItemStack stack : sublist)
		{
			itemDmg = stack.getItemDamage();
			if (itemDmg>dmgMax)
				dmgMax = itemDmg;
			if (itemDmg<dmgMin)
				dmgMin = itemDmg;
		}
		ItemRange irange = new ItemRange(item.itemID, dmgMin, dmgMax);
		for (ItemStack stack : sublist)
		{
			if (stack.hasTagCompound())
				irange.addItemIfInRange(stack.itemID, stack.getItemDamage(), stack.getTagCompound());
		}
		return irange;

	}
	public static ItemStack getItemStackByFieldName(Class cls, Object instance, String fieldName)
	{
		Object result = null;
		try
		{
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);
			result = field.get(instance);
		}
		catch (NoSuchFieldException e)
		{
			NEIPlugins.log(Level.FINE,"Failed to get field \"{1}\" from class \"{0}\"", new Object[] {cls.getCanonicalName(), fieldName});
			return null;
		}
		catch (Exception e)
		{
			NEIPlugins.log(Level.FINE,"Failed to get field \"{1}\" from class \"{0}\": {2}", new Object[] {cls.getCanonicalName(), fieldName, e.getMessage()});
			return null;
		}
		if (result == null)
			return null;

		if (result instanceof Item)
			return new ItemStack((Item)result);
		else if (result instanceof Block)
			return new ItemStack((Block)result);
		else if (result instanceof ItemStack)
			return (ItemStack)result;
		else
			NEIPlugins.log(Level.FINE,"Unknown class type for field \"{1}\" from class \"{0}\": {2}", new Object[] {cls.getCanonicalName(), fieldName, result.getClass().getCanonicalName()});
		return null;
	}


	public static ItemRange getItemRangeByFieldName(Class cls, Object instance, String fieldName, boolean useNBT, boolean requireDamage)
	{
		Object result = null;
		try
		{
			Field field = cls.getDeclaredField(fieldName);
			field.setAccessible(true);
			result = field.get(instance);
		}
		catch (NoSuchFieldException e)
		{
			NEIPlugins.log(Level.FINE,"Failed to get field \"{1}\" from class \"{0}\"", new Object[] {cls.getCanonicalName(), fieldName});
			return null;
		}
		catch (Exception e)
		{
			NEIPlugins.log(Level.FINE,"Failed to get field \"{1}\" from class \"{0}\": {2}", new Object[] {cls.getCanonicalName(), fieldName, e.getMessage()});
			return null;
		}
		if (result == null)
			return null;
		ItemRange irange = null;

		if (result instanceof Item)
			irange = new ItemRange(((Item)result).itemID);
		else if (result instanceof Block)
			irange = new ItemRange(((Block)result).blockID);
		else if (result instanceof ItemStack) {
			ItemStack item = (ItemStack) result;
			if (item.getItem().isDamageable())
				irange = new ItemRange(item.itemID);
			else
				irange = new ItemRange(item.itemID, item.getItemDamage(), item.getItemDamage());

			if (useNBT && ((ItemStack)result).hasTagCompound())
				irange.addItemIfInRange(irange.firstID, irange.firstDamage, item.getTagCompound());
		} else {
			NEIPlugins.log(Level.FINE,"Unknown class type for field \"{1}\" from class \"{0}\": {2}", new Object[] {cls.getCanonicalName(), fieldName, result.getClass().getCanonicalName()});
			return null;
		}
		if (requireDamage && irange.firstID == irange.lastID && irange.firstDamage == -1) {
			irange.firstDamage = 0;
			irange.lastDamage = 0;
		}
		return irange;
	}

}