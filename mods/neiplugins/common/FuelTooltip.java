package mods.neiplugins.common;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidStack;

import mods.neiplugins.NEIPlugins;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.Utils;

public abstract class FuelTooltip
{
	public static DecimalFormat heatFormat = new DecimalFormat("0.#");
	public static DecimalFormat MJtFormat = new DecimalFormat("0.0");
	public static final String linePrefix = "\u00a77";

	public static Map<String, IFuelHelper> registredHelpers = new HashMap<String, IFuelHelper>();
	public static Map<String, IFuelContextHelper> contextHelpers = new HashMap<String, IFuelContextHelper>();
	public static List<String> helpersPosition = new ArrayList<String>();
	public static Set<String> disabledHelpers = new HashSet<String>();
	public static Set<String> disabledContextHelpers = new HashSet<String>();
	public static Set<Integer> allowedNBTItems = new HashSet<Integer>();

	public static void addFuelHelper(boolean hide, IFuelHelper helper) {
		String key = helper.getKey();
		if (!registredHelpers.containsKey(key))
			registredHelpers.put(key, helper);
		if (!helpersPosition.contains(key))
			helpersPosition.add(key);
		if (hide)
			disabledHelpers.add(key);
	}

	public static void addFuelHelper(IFuelHelper helper) {
		addFuelHelper(false, helper);
	}

	public static void addContextFuelHelper(IFuelContextHelper helper) {
		String key = helper.getKey();
		if (!contextHelpers.containsKey(key))
			contextHelpers.put(key, helper);
	}

	public static void addValidNBTItem(ItemStack stack) {
		if (stack == null)
			return;
		int key = Utils.itemStackToInt(stack);
		if (key != 0)
			allowedNBTItems.add(key);
	}

	public static boolean isValidFuelItem(ItemStack stack) {
		return stack != null && (!stack.hasTagCompound() || allowedNBTItems.contains(Utils.itemStackToInt(stack)));
	}


	public static void addLiquidStackFuelTooltip(List<String> currenttip, LiquidStack liquid, Set<String> disabledHelpers) {
		if (liquid == null)
			return;
		for (String key : helpersPosition)
		{
			if (!disabledHelpers.contains(key))
				currenttip = registredHelpers.get(key).getLiquidStackFuelTooltip(liquid, currenttip);
		}
	}

	public static void addItemStackFuelTooltip(List<String> currenttip, ItemStack stack, Set<String> disabledHelpers) {
		if (stack == null)
			return;
		for (String key : helpersPosition)
		{
			if (!disabledHelpers.contains(key))
				currenttip = registredHelpers.get(key).getItemStackFuelTooltip(stack, currenttip);
		}
	}

	protected static boolean showContextTooltip;
	protected static boolean showLiquiqAsItemFuelTooltip;
	protected static boolean showLiquidContainerFuelTooltip;
	protected static boolean showItemStackFuelTooltip;

	public static void addCommonFuelTooltip(List<String> currenttip, ItemStack stack, Set<String> disabledHelpers) {
		if (stack == null)
			return;
		for (String key : helpersPosition)
		{
			if (!disabledHelpers.contains(key))
			{
				try {
					if (showItemStackFuelTooltip)
						currenttip = registredHelpers.get(key).getItemStackFuelTooltip(stack, currenttip);

					LiquidStack liquid = null;
					if (showLiquidContainerFuelTooltip)
						liquid = LiquidContainerRegistry.getLiquidForFilledItem(stack);

					if (showLiquiqAsItemFuelTooltip && liquid == null) {
						liquid = new LiquidStack(stack.itemID, 1000, stack.getItemDamage());
						if (!LiquidHelper.isLiquid(liquid))
							liquid = null;
					}
					if (liquid != null)
						currenttip = registredHelpers.get(key).getLiquidStackFuelTooltip(liquid, currenttip);
				} catch (Exception ex) {
					NEIPlugins.logWarningEx("Error occurred while calling FuelTooltip.addCommonFuelTooltip for fuel helper {0} and item {1}", ex, key, stack);
					currenttip.add("\u00a7c<Error occured>");
				}
			}
		}
	}

	public static void addHighlightFuelTooltip(List<String> currenttip, ItemStack stack, Set<String> disabledHelpers, String hKey) {
		if (stack == null)
			return;
		for (String key : helpersPosition)
		{
			try {
				if (key.equals(hKey)) {
					List<String> templist = new ArrayList<String>();
					templist = registredHelpers.get(key).getItemStackFuelTooltip(stack, templist);
					LiquidStack liquid = LiquidHelper.getLiquidStack(stack);
					if (liquid != null)
						templist = registredHelpers.get(key).getLiquidStackFuelTooltip(liquid, templist);
					for (String line : templist)
						if (line.startsWith(linePrefix))
							currenttip.add("\u00a7e"+line.substring(2));
						else
							currenttip.add(line);

				} else if (!disabledHelpers.contains(key)) {
					if (showItemStackFuelTooltip)
						currenttip = registredHelpers.get(key).getItemStackFuelTooltip(stack, currenttip);

					LiquidStack liquid = null;
					if (showLiquidContainerFuelTooltip) {
						liquid = LiquidContainerRegistry.getLiquidForFilledItem(stack);
					}
					if (showLiquiqAsItemFuelTooltip && liquid == null) {
						liquid = new LiquidStack(stack.itemID, 1000, stack.getItemDamage());
						if (!LiquidHelper.isLiquid(liquid))
							liquid = null;
					}
					if (liquid != null)
						currenttip = registredHelpers.get(key).getLiquidStackFuelTooltip(liquid, currenttip);
				}
			} catch (Exception ex) {
				NEIPlugins.logWarningEx("Error occurred while calling FuelTooltip.addHighlightFuelTooltip for fuel helper {0} and item {1}", ex, key, stack);
				currenttip.add("\u00a7c<Error occured>");
			}
		}
	}

	protected static Class lastGuiContainer = null;
	protected static TooltipCache lastContextCache = new TooltipCache();
	public static TooltipCache getContextCache(GuiContainer gui) {
		if (lastGuiContainer != gui.getClass()) {
			lastContextCache.clear();
			lastGuiContainer = gui.getClass();
		}
		return lastContextCache;
	}

	public static void addContextTooltipCached(GuiContainer gui, List<String> currenttip, ItemStack stack, String hKey)
	{
		addContextTooltipCached(gui, currenttip, stack, disabledHelpers, hKey);
	}

	public static void addContextTooltipCached(GuiContainer gui, List<String> currenttip, ItemStack stack, Set<String> disabledHelpers, String hKey)
	{
		TooltipCache tooltipCache = getContextCache(gui);

		if (tooltipCache.addCachedTooltip(stack, currenttip) || !isValidFuelItem(stack))
			return;

		List<String> fueltooltip = new ArrayList<String>();
		FuelTooltip.addHighlightFuelTooltip(fueltooltip, stack, disabledHelpers, hKey);

		if (fueltooltip.size()>0) {
			fueltooltip.add(0,"\u00a77Can produce:");
			currenttip.addAll(fueltooltip);
		}

		if (!stack.hasTagCompound())
		{
			if (fueltooltip.size()>0)
				tooltipCache.put(stack, fueltooltip);
			else
				tooltipCache.put(stack, null);
		}
	}

	public static void updateSettings() {
		showContextTooltip = Utils.getBooleanSetting("mistaqur.showContextTooltip");
		showLiquidContainerFuelTooltip = Utils.getBooleanSetting("mistaqur.showLiquidContainerFuelTooltip");
		showLiquiqAsItemFuelTooltip = Utils.getBooleanSetting("mistaqur.showLiquiqAsItemFuelTooltip");
		showItemStackFuelTooltip = Utils.getBooleanSetting("mistaqur.showItemStackFuelTooltip");
		FuelTooltipHandler.tooltipCache.clear();
	}

	public static String compactValueF(float value) {
		if (value > 99)
			return heatFormat.format(Math.round(value / 100.0f)/10.0f) + "k";
		return Integer.toString(Math.round(value));
	}

	public static String compactValue(int value) {
		if (value > 99 && (value%100 == 0))
			return heatFormat.format(value / 1000.0f) + "k";
		return Integer.toString(value);
	}

	public static String convertHeatValue(int value, int second) {
		return compactValue(value)+" heat";
	}

	public static String convertMJt(int value)
	{
		return MJtFormat.format(value);
	}

	public static String convertMJtF(float value)
	{
		return MJtFormat.format(value);
	}

}