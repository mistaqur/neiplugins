package mods.neiplugins.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;

import mods.neiplugins.NEIPlugins;

public abstract class LiquidHelper
{
	public static boolean isSafeLiquidStack(LiquidStack liquid, String source)
	{
		if (liquid == null)
			return false;
		if (Item.itemsList[liquid.itemID] == null) {
			NEIPlugins.log(Level.WARNING,"Detected liquid {0}:{1} with invalid itemID in \"{2}\"", liquid.itemID, liquid.itemMeta, source);
			return false;
		}
		LiquidStack canonical = liquid.canonical();
		if (canonical == null) {
			NEIPlugins.log(Level.WARNING,"Detected liquid {0}:{1} that don't have canonical liquid in \"{2}\"", liquid.itemID, liquid.itemMeta, source);
			return false;
		}
		return true;

	}
	public static String getLiquidName(LiquidStack liquid) {
		if (Item.itemsList[liquid.itemID] == null) {
			NEIPlugins.log(Level.WARNING,"Detected liquid {0}:{1} with invalid itemID", liquid.itemID, liquid.itemMeta);
			return "";
		}

		String res = Item.itemsList[liquid.itemID].getItemDisplayName(liquid.asItemStack());
		if (res.isEmpty()) {
			res = getCanonicalLiquidName(liquid);
			if (!res.isEmpty())
				res = "\u00a73" + res;
		}
		return res;
	}

	public static String getCanonicalLiquidName(LiquidStack liquid) {
		return LiquidDictionary.findLiquidName(liquid);
	}

	public static Icon bindLiquidTexture(GuiContainerManager gui, LiquidStack liquid)
	{
		if ((liquid == null) || (liquid.amount <= 0) || (liquid.itemID == 0))
			return null;

		Icon liquidIcon = null;
		String texture = "/terrain.png";

		LiquidStack canonical = liquid.canonical();
		if (canonical != null) {
			liquidIcon = canonical.getRenderingIcon();
			texture = canonical.getTextureSheet();
			liquid = canonical;
		}

		if (texture == null)
			texture = "/terrain.png";

		if (liquidIcon == null) {
			if ((liquid.itemID < Block.blocksList.length) && (Block.blocksList[liquid.itemID] != null)) {
				liquidIcon = Block.blocksList[liquid.itemID].getBlockTextureFromSide(0);
			} else {
				liquidIcon = Item.itemsList[liquid.itemID].getIconFromDamage(liquid.itemMeta);
				texture = "/gui/items.png";
			}
		}

		gui.bindTexture(texture);
		Item liquidItem = liquid.asItemStack().getItem();
		if (liquidItem != null) {
	                int color = liquidItem.getColorFromItemStack(liquid.asItemStack(), 0);
	                float r = (float)(color >> 16 & 0xff) / 255F;
        	        float g = (float)(color >> 8 & 0xff) / 255F;
                	float b = (float)(color & 0xff) / 255F;
			GL11.glColor4f(r, g, b, 1.0F);
		} else {
			GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0F);
		}
		return liquidIcon;
	}

	public static boolean isLiquid(LiquidStack test)
	{
		if (test == null)
			return false;
		//if (LiquidContainerRegistry.isLiquid(test))
		//	return true;

		if (LiquidDictionary.findLiquidName(test)!=null)
			return true;
		return false;
	}

	public static LiquidStack getLiquidStack(ItemStack item)
	{
		if (item == null)
			return null;

		LiquidStack test = new LiquidStack(item.itemID, 1000, item.getItemDamage());

		if (LiquidContainerRegistry.isLiquid(item))
			return test;

		if (LiquidDictionary.findLiquidName(test)!=null)
			return test;

		LiquidStack result = LiquidContainerRegistry.getLiquidForFilledItem(item);
		if (result != null)
			return result;

		return null;
	}

	public static PositionedStack[] generateStacksForLiquid(LiquidStack liquid, int x1, int y1, int x2, int y2)
	{
		if (mapFilledContainersFromLiquid.isEmpty())
			generateLiquidCache();
		List key = Arrays.asList(liquid.itemID, liquid.itemMeta);
		if (!mapFilledContainersFromLiquid.containsKey(key)) {
			ItemStack stack = liquid.asItemStack();
			if (Utils.isSafeItemStack(stack, "LiquidHelper.generateStacksForLiquid"))
				return new PositionedStack[] {null, new PositionedStack(stack,x2,y2)};
			else
				return new PositionedStack[] {null, null};
		}
		return new PositionedStack[] {new PositionedStack(mapEmptyContainersFromLiquid.get(key),x1,y1), new PositionedStack(mapFilledContainersFromLiquid.get(key),x2,y2)};
	}

	public static boolean areSameLiquid(LiquidStack liquid, LiquidStack liquid2)
	{
		if (liquid == null || liquid2 == null)
		{
			return false;
		}
		return liquid.isLiquidEqual(liquid2);
	}

	public static PositionedStack getSeqCycledStack(int cycle, PositionedStack origstack)
	{
		if (origstack == null)
			return null;
		PositionedStack stack = origstack.copy();
		if(stack.items.length > 1)//cycle items
		{
			stack.setPermutationToRender(cycle%stack.items.length);
		}
		return stack;
	}

	public static void generateLiquidCache()
	{
		mapFilledContainersFromLiquid.clear();
		mapEmptyContainersFromLiquid.clear();
		for (LiquidContainerData data : LiquidContainerRegistry.getRegisteredLiquidContainerData())
		{
			List key = Arrays.asList(data.stillLiquid.itemID, data.stillLiquid.itemMeta);
			if (!mapFilledContainersFromLiquid.containsKey(key)) {
				mapFilledContainersFromLiquid.put(key, new ArrayList<ItemStack>());
				mapEmptyContainersFromLiquid.put(key, new ArrayList<ItemStack>());
			}
			if (Utils.isSafeItemStack(data.filled.copy(), "LiquidContainerRegistry.getRegisteredLiquidContainerData[].filled") &&
			    Utils.isSafeItemStack(data.container.copy(), "LiquidContainerRegistry.getRegisteredLiquidContainerData[].container")) {
				mapFilledContainersFromLiquid.get(key).add(data.filled.copy());
				mapEmptyContainersFromLiquid.get(key).add(data.container.copy());
			}
		}
	}

	private static Map<List, ArrayList<ItemStack>> mapFilledContainersFromLiquid = new HashMap();
	private static Map<List, ArrayList<ItemStack>> mapEmptyContainersFromLiquid = new HashMap();

}