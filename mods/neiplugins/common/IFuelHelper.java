package mods.neiplugins.common;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

public interface IFuelHelper {
	public String getName();
	public String getKey();
	public List<String> getDescription();
	public List<String> getLiquidStackFuelTooltip(LiquidStack liquid, List<String> currenttip);
	public List<String> getItemStackFuelTooltip(ItemStack stack, List<String> currenttip);
}