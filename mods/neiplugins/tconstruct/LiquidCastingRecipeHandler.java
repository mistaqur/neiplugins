package mods.neiplugins.tconstruct;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.forge.GuiContainerManager;

import mods.tinker.tconstruct.library.TConstructRegistry;
import mods.tinker.tconstruct.library.crafting.CastingRecipe;
import mods.tinker.tconstruct.library.crafting.LiquidCasting;

public class LiquidCastingRecipeHandler extends CastRecipeHandler
{
	@Override
	public String getRecipeName()
	{
		return "Liquid Table Casting";
	}

	public String getRecipeId()
	{
		return "tconstruct.liquidcast";
	}

	public ArrayList<CastingRecipe> getCastingRecipes()
	{
		LiquidCasting t = TConstructRegistry.getTableCasting();
		if (t == null)
			return new ArrayList<CastingRecipe>();
		return t.getCastingRecipes();
	}

	@Override
	public void drawBackground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.bindTexture(getGuiTexture());
		gui.drawTexturedModalRect(xOffset, 0, 0, 0, 112, 55);
	}
}