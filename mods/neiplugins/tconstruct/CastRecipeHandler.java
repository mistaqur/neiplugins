package mods.neiplugins.tconstruct;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;

import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;
import mods.neiplugins.common.LiquidTemplateRecipeHandler;
import mods.tinker.tconstruct.library.crafting.CastingRecipe;

public abstract class CastRecipeHandler extends LiquidTemplateRecipeHandler
{
	public static final int xOffset = 30;
	static Rectangle metalTank = new Rectangle (xOffset+30,8,6,11);

	public class CachedCastingRecipe extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		PositionedStack cast = null;
		LiquidTank metal;
		PositionedStack output = null;

		public CachedCastingRecipe(CastingRecipe irecipe)
		{
			metal = new LiquidTank(irecipe.castingMetal,irecipe.castingMetal.amount,metalTank);
			metal.showMillBuckets = true;
			metal.showCapacity = false;
			metal.showAmount = true;
			if (irecipe.cast != null)
				cast = new PositionedStack(irecipe.cast, xOffset + 25, 19);
			output = new PositionedStack(irecipe.output, xOffset + 80, 18);
		}

		@Override
		public PositionedStack getResult()
		{
			return output;
		}

		@Override
		public PositionedStack getIngredient()
		{
			return cast;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			ArrayList<LiquidTank> res = new ArrayList<LiquidTank>();
			res.add(metal);
			return res;
		}
	}

	@Override
	public String getRecipeName()
	{
		return "Coke Oven";
	}

	@Override
	public String getRecipeId()
	{
		return "Coke Oven";
	}

	public abstract ArrayList<CastingRecipe> getCastingRecipes();

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(xOffset + 46, 18, 22, 15), getRecipeId()));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals(getRecipeId()))
		{
			for(CastingRecipe recipe : getCastingRecipes())
			{
				arecipes.add(new CachedCastingRecipe(recipe));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	@Override
	public void loadUsageRecipes(String inputId, Object... ingredients)
	{
		if (inputId.equals("liquid") && ingredients.length == 1 && ingredients[0] instanceof LiquidStack)
		{
			LiquidStack t = (LiquidStack)ingredients[0];
			for(CastingRecipe recipe : getCastingRecipes())
			{
				if(recipe.castingMetal.isLiquidEqual(t))
					arecipes.add(new CachedCastingRecipe(recipe));
			}
		}
		else
		{
			super.loadUsageRecipes(inputId, ingredients);
		}
	}

	public void loadUsageRecipes(ItemStack ingred)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(ingred);
		for(CastingRecipe recipe : getCastingRecipes())
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(recipe.cast, ingred) || t != null && recipe.castingMetal.isLiquidEqual(t))
			{
				arecipes.add(new CachedCastingRecipe(recipe));
			}
		}
	}

	public void loadCraftingRecipes(ItemStack result)
	{
		for(CastingRecipe recipe : getCastingRecipes())
		{
			if(NEIClientUtils.areStacksSameType(result, recipe.getResult()))
			{
				arecipes.add(new CachedCastingRecipe(recipe));
			}
		}
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/neiplugins/gfx/tc_cast.png";
	}
}