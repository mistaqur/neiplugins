package mods.neiplugins.tconstruct;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.forge.GuiContainerManager;

import mods.tinker.tconstruct.library.TConstructRegistry;
import mods.tinker.tconstruct.library.crafting.CastingRecipe;
import mods.tinker.tconstruct.library.crafting.LiquidCasting;

public class LiquidBlockCastingRecipeHandler extends CastRecipeHandler
{
	@Override
	public String getRecipeName()
	{
		return "Liquid Basin Casting";
	}

	public String getRecipeId()
	{
		return "tconstruct.liquidblockcast";
	}

	public ArrayList<CastingRecipe> getCastingRecipes()
	{
		LiquidCasting t = TConstructRegistry.getBasinCasting();
		if (t == null)
			return new ArrayList<CastingRecipe>();
		return t.getCastingRecipes();
	}

	@Override
	public void drawBackground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.bindTexture(getGuiTexture());
		gui.drawTexturedModalRect(xOffset, 0, 0, 62, 112, 55);
	}
}